package main

import (
	"github.com/volatiletech/authboss/v3"
	"github.com/volatiletech/authboss/v3/defaults"
)

type MyDatabaseImplementation struct {
}

//func (MyDatabaseImplementation) Load(ctx context.Context, key string) (User, error) {
//	//...
//}
//
//func (MyDatabaseImplementation) Save(ctx context.Context, user User) error {
//	//...
//}

func main() {
	ab := authboss.New()

	ab.Config.Paths.Mount = "/authboss"
	ab.Config.Paths.RootURL = "https://www.example.com/"

	//ab.Config.Storage.Server = MyDatabaseImplementation{}
	//ab.Config.Storage.SessionState = MySessionImplementation{}
	//ab.Config.Storage.CookieState = MyCookieImplementation{}
	//
	//// This is using the renderer from: github.com/volatiletech/authboss
	//ab.Config.Core.ViewRenderer = abrenderer.NewHTML("/auth", "ab_views")
	// Probably want a MailRenderer here too.

	// This instantiates and uses every default implementation
	// in the Config.Core area that exist in the defaults package.
	// Just a convenient helper if you don't want to do anything fancy.
	defaults.SetCore(&ab.Config, false, false)

	if err := ab.Init(); err != nil {
		panic(err)
	}
}
