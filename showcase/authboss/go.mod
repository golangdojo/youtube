module golangdojo.com/golangdojo/authboss

go 1.18

require (
	github.com/friendsofgo/errors v0.9.2 // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/volatiletech/authboss/v3 v3.2.0 // indirect
	golang.org/x/crypto v0.0.0-20220817201139-bc19a97f63c8 // indirect
	golang.org/x/net v0.0.0-20220812174116-3211cb980234 // indirect
	golang.org/x/oauth2 v0.0.0-20220808172628-8227340efae7 // indirect
	golang.org/x/xerrors v0.0.0-20220609144429-65e65417b02f // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
