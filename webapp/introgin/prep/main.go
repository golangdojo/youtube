package main

import (
	"github.com/gin-gonic/gin"
	"log"
)

func HelloNinja(c *gin.Context) {
	c.JSON(200, gin.H{
		"message": "Hello Ninja!",
	})
}

func GetWeapon(c *gin.Context) {
	weaponType := c.Query("type")
	weaponName, ok := ninjaWeapons[weaponType]
	if !ok {
		c.JSON(404, gin.H{
			weaponType: "",
		})
		return
	}

	c.JSON(200, gin.H{
		weaponType: weaponName,
	})
}

func PostWeapon(c *gin.Context) {
	weaponType := c.Query("type")
	weaponName := c.Query("name")

	if len(weaponType) == 0 || len(weaponName) == 0 {
		c.JSON(400, gin.H{
			weaponType: weaponName,
		})
		return
	}

	if _, ok := ninjaWeapons[weaponType]; ok {
		c.JSON(409, gin.H{
			"message": "Weapon already exists",
		})
		return
	}

	ninjaWeapons[weaponType] = weaponName
	c.JSON(201, gin.H{
		weaponType: weaponName,
	})
}

func DeleteWeapon(c *gin.Context) {
	weaponType := c.Query("type")
	weaponName, ok := ninjaWeapons[weaponType]
	if !ok {
		c.JSON(404, gin.H{
			weaponType: "",
		})
		return
	}

	delete(ninjaWeapons, weaponType)
	c.JSON(200, gin.H{
		weaponType: weaponName,
	})
}

var ninjaWeapons = map[string]string{
	"ninjaSword": "Beginner Ninja Sword - Damage 5",
}

func main() {
	//r := gin.New()
	r := gin.Default()
	r.GET("/", HelloNinja)

	//r.GET("/weapon", GetWeapon)
	//r.POST("/weapon", PostWeapon)
	//r.DELETE("/weapon", DeleteWeapon)
	rGroup := r.Group("/weapon")
	rGroup.GET("", GetWeapon)
	rGroup.POST("", PostWeapon)
	rGroup.DELETE("", DeleteWeapon)

	err := r.Run() // listen and serve on 0.0.0.0:8080
	if err != nil {
		log.Fatal(err)
	}
}
