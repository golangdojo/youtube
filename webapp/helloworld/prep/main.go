package main

import (
	"fmt"
	"log"
	"net/http"
)

func helloWorldPage(w http.ResponseWriter, r *http.Request) {
	log.Println(fmt.Fprint(w, "Hello World"))
}

func main() {
	http.HandleFunc("/", helloWorldPage)
	log.Fatal(http.ListenAndServe("", nil))
}
