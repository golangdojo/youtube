package main

import (
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/fiber/v2/middleware/requestid"
)

type Ninja struct {
	Name string `json:"name"`
	Weapon string `json:"weapon"`
}

var ninja Ninja

//func getNinja(ctx *fiber.Ctx) error {
//	wallace := Ninja{"Wallace", "Ninja Star"}
//	return ctx.Status(fiber.StatusOK).JSON(wallace)
//}

func getNinja(ctx *fiber.Ctx) error {
	return ctx.Status(fiber.StatusOK).JSON(ninja)
}

func createNinja(ctx *fiber.Ctx) error {
	newNinja := new(Ninja)
	err := ctx.BodyParser(newNinja)
	// select json in postman

	if err != nil {
		ctx.Status(fiber.StatusBadRequest).SendString(err.Error())
		return err
	}

	ninja = Ninja{
		Name: newNinja.Name,
		Weapon: newNinja.Weapon,
	}
	return ctx.Status(fiber.StatusOK).JSON(newNinja)
}

func main() {
	app := fiber.New()

	//app.Get("/", func(c *fiber.Ctx) error {
	//	return c.SendString("Hello World")
	//})
	//app.Get("/ninja", getNinja)
	//app.Post("/ninja", createNinja)
	//app.Listen(":80")

	// middleware - logger, make sure to put this first or it won't show
	app.Use(logger.New())
	// middleware - request id, default is util.UUID
	app.Use(requestid.New())
	app.Get("/", func(c *fiber.Ctx) error {
		return c.SendString("Hello World")
	})

	// grouping prefix
	ninjaApp := app.Group("/ninja")
	ninjaApp.Get("", getNinja)
	ninjaApp.Post("", createNinja)

	app.Listen(":80")
}
