package main

import (
	"fmt"
	"html/template"
	"net/http"
	"strings"
)

func login(w http.ResponseWriter, r *http.Request) {
	var fileName = "login.html"
	t, err := template.ParseFiles(fileName)
	if err != nil {
		fmt.Println("Error when parsing file", err)
		return
	}
	type Ninja struct {
		Name string
		Level int
	}

	err = t.ExecuteTemplate(w, fileName, Ninja{"Wallace", 9001})
	if err != nil {
		fmt.Println("Error when executing template", err)
		return
	}
}

var userDB = map[string]string {
	"Wallace": "goodPassword",
}

func loginSubmit(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")

	if userDB[username] == password {
		w.WriteHeader(http.StatusOK)
		fmt.Fprintf(w, "You've now logged in. Welcome to Golang Dojo")
	} else {
		w.WriteHeader(http.StatusNotFound)
		fmt.Fprintf(w, "Didn't find matching credential")
	}

}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/login": // todo: input boxes & button for credentials
		login(w, r)
	case "/login-submit": // todo: handle login credentials
		loginSubmit(w, r)
	case "/function":
		fileName := "function.html"
		funcMap := map[string]interface{} {
			"upper": strings.ToUpper,
		}
		t, err := template.New(fileName).Funcs(funcMap).ParseFiles(fileName)
		if err != nil {
			fmt.Println(err)
			return
		}
		err = t.ExecuteTemplate(w, fileName, "Hello Ninjas")
		if err != nil {
			fmt.Println(err)
			return
		}
	default:
		fmt.Fprintf(w, "Sup Ninjas")
	}
}
func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe("", nil)
	//http.ListenAndServeTLS("", "cert.pem", "key.pem", nil)
	//go run $(go env GOROOT)/src/crypto/tls/generate_cert.go --host=localhost
}
