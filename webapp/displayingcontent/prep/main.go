package main

import (
	"fmt"
	"html/template"
	"net/http"
)

var fileName = "login.html"

func login(w http.ResponseWriter, r *http.Request) {
	display(w, "Please login")
}

func loginSubmit(w http.ResponseWriter, r *http.Request) {
	username := r.FormValue("username")
	password := r.FormValue("password")
	w.WriteHeader(http.StatusOK)
	fmt.Fprint(w, "You've logged in. Welcome to Golang Dojo")
	fmt.Printf("Logged in with username={%s}, password={%s}", username, password)
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/login":
		login(w, r)
	case "/login-submit":
		loginSubmit(w, r)
	case "/home":
		fmt.Fprint(w, "Welcome to Golang Dojo")
	case "/function":
		//funcMap := map[string]interface{} {
		//	"upper": strings.ToUpper,
		//}
		//t, err := template.New()Funcs(funcMap).ParseFiles("function.html")
		//if err != nil {
		//	fmt.Println("Error parsing file:", err)
		//	return
		//}
		//err = t.ExecuteTemplate(w, "function.html", "Hello Ninjas")
		//if err != nil {
		//	fmt.Println("Error executing template:", err)
		//	return
		//}
	default:
		w.WriteHeader(http.StatusNotFound)
	}
}

func display(w http.ResponseWriter, data interface{}) {
	t, err := template.ParseFiles(fileName)
	if err != nil {
		fmt.Println("Error parsing file:", err)
		return
	}
	err = t.ExecuteTemplate(w, fileName, data)
	if err != nil {
		fmt.Println("Error executing template:", err)
		return
	}
}

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe("", nil)
	//http.ListenAndServeTLS("", "cert.pem", "key.pem", nil)
	// leave a comment if you want a video on https
	// even though it's not technically quite directly related to Go
	//go run $(go env GOROOT)/src/crypto/tls/generate_cert.go --host=localhost
}
