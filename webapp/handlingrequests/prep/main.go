package main

import (
	"fmt"
	"net/http"
	"time"
)

// timestamp - Request Routing
// timestamp - Request Method
func handleFunction(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/":
		fmt.Fprint(w, "Hello World")
	case "/ninja":
		fmt.Fprint(w, "Wallace")
	default:
		fmt.Fprint(w, "Big Fat Error!")
	}
	fmt.Printf("HandleFunction with %s request\n", r.Method)
	// will talk more about the different methods and
	// how to make full use of them in the future about REST APIs
	// same goes for the other many other components in both of the request and response writer
}

// timestamp - Response Header
func htmlVsPlain(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<h1>Hello World</h1>")
	w.Header().Set("Content-Type", "text/plain")
	w.Header().Set("Content-Type", "text/html")
	// manually set w.Header().Set("User-Agent", "Mozilla/5.0...")
}

func timeout(w http.ResponseWriter, r *http.Request) {
	//fmt.Println("Timeout attempt")
	time.Sleep(3 * time.Second)
	fmt.Fprint(w, "Did *not* timeout")
}

func helloWorldNinjaMode(w http.ResponseWriter, r *http.Request) {
	fmt.Fprint(w, "<body style=\"background-color:grey;\">Hello World</body>")
}

func main() {
	http.HandleFunc("/", handleFunction)
	http.HandleFunc("/timeout", timeout)

	// timestamp - Server Configs

	//expanding http.ListenAndServe("", nil)
	server := http.Server {
		Addr:         "",
		Handler:      nil, // 404 anywhere if there isn't a default
		ReadTimeout:  1000,
		WriteTimeout: 1000,
	}
	server.ListenAndServe() // can be wrapped with log.Fatal

	// timestamp - Multiplexer

	// if local time is past 10PM
	var muxNinjaMode http.ServeMux
	muxNinjaMode.HandleFunc("/", helloWorldNinjaMode)
	server.Handler = &muxNinjaMode
	server.ListenAndServe()
	//mention other muxers like gorilla mux which allows
	//more pattern matching, future videos

}
