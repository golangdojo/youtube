package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"strconv"
)

func main() {
	/*
		TCP(Transmission Control Protocol) is a network protocol which
		is used on the Internet and similar networks. TCP allows
		computers to send arbitrary length data to each other with
		guaranteed delivery by first establishing a formal handshake

		UDP(User Datagram Protocol) is a network protocol for time-sensitive
		transmissions. It speeds up communications by not formally
		establishing a connection before data is transferred.

		HTTP is an application-layer (as supposed to transport-layer
		like TCP/UDP) network protocol that runs over TCP. For all intents
		and purposes, TCP/UDP allows us to transfer data and HTTP allows
		us to specify the data we want by attaching more info to the request

		RPC(Remote Procedure Call) basically is a form of inter-process
		communication that allows one program to directly call procedures
		in another program either on the same machine or another machine
		on the network. RPC can run on TCP or UDP.

		REST
	*/

	listener, err := net.Listen("tcp", ":9001")
	if err != nil {
		log.Panic(err)
	}
	defer closeListener(listener)

	for i := 0; ; i++ {
		conn, err := listener.Accept()
		if err != nil {
			log.Panic(err)
		}
		log.Println("Received request...")
		response := fmt.Sprintf("Response #%v", strconv.Itoa(i))
		io.WriteString(conn, response)
		if err := conn.Close(); err != nil {
			log.Panic(err)
		}
	}

}

func closeListener(listener net.Listener) {
	err := listener.Close()
	if err != nil {
		log.Panic(err)
	}
}
