package main

import (
	"log"
	"net"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:9001")
	if err != nil {
		log.Panic(err)
	}
	response := make([]byte, 50)
	conn.Read(response)
	log.Println(string(response))
	defer closeConnection(conn)
}

func closeConnection(conn net.Conn) {
	err := conn.Close()
	if err != nil {
		log.Panic(err)
	}
}
