package main

import (
	"google.golang.org/grpc"
	"log"
	"net"
)

func main() {
	// gRPC is a modern open source high performance Remote Procedure Call (RPC) framework

	// Remote Procedure Call (RPC) is a communication technique that allows an application
	// to interact with services on a remote computer on the (same) network.

	// It's similar to the REST APIs we've built in the golang web app series
	// Differences:
	// 1. REST uses HTTP1; RPC uses HTTP2 (typically faster but with less support)
	// 2. REST uses GET/POST/PUT/DELETE/etc.; RPC uses GET/POST (can still update and delete)

	// Overview
	// Package net
	// grpc
	// protobuff

	// go get -u google.golang.org/grpc

	listener, err := net.Listen("tcp", ":9001")
	if err != nil {
		log.Panic("Failed to listen on port over 9000!!!")
	}
	grpcServer := grpc.NewServer()
	if err := grpcServer.Serve(listener); err != nil {
		log.Panic("Failed to serve gRPC server on port over 9000!!!")
	}

	// proto buff
	// go get -u google.golang.org/protobuf/protoc-gen
}
