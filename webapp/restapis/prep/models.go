package main

import (
	"strconv"
	"time"
)













type Ninja struct {
	Id     string
	Name   string
	Level  int
	Status NinjaStatus
}










type Dojo struct {
	Id               string
	Name             string
	AssociatedNinjas []Ninja
	Status           DojoStatus
}








var ninjaDB = NinjaDB{}
var dojoDB = NinjaDB{}

type NinjaDB map[string]Ninja
type DojoDB map[string]Dojo

type NinjaStatus string

type DojoStatus string

const (
	NinjaStatusActive = "ACTIVE"
	NinjaStatusQuit   = "QUIT"
)

func (NinjaDB) Get(id string) (Ninja, bool) {
	ninja, ok := ninjaDB[id]
	if !ok {
		return Ninja{}, false
	}
	return ninja, true
}

func (NinjaDB) GetAll() []Ninja {
	var ninjas []Ninja
	for _, ninja := range ninjaDB {
		ninjas = append(ninjas, ninja)
	}
	return ninjas
}

func (NinjaDB) New(name string) Ninja {
	var id string
	for {
		id = strconv.FormatInt(time.Now().UnixNano(), 10)
		_, ok := ninjaDB.Get(id)
		if !ok {
			break
		}
	}

	ninja := Ninja{
		Id:     id,
		Name:   name,
		Level:  0,
		Status: NinjaStatusActive,
	}
	ninjaDB[id] = ninja
	return ninja
}

func (NinjaDB) Update(newNinja Ninja) (Ninja, bool) {
	_, ok := ninjaDB.Get(newNinja.Id)
	if !ok {
		return Ninja{}, false
	}
	ninjaDB[newNinja.Id] = newNinja
	return newNinja, true
}

func (NinjaDB) Delete(id string) (Ninja, bool) {
	ninja, ok := ninjaDB.Get(id)
	if !ok {
		return Ninja{}, false
	}

	delete(ninjaDB, id)
	return ninja, true
}
