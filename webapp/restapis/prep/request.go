package main

type GetNinjaRequest struct {
	Id string
}

type CreateNinjaRequest struct {
	Name string
}

type UpdateNinjaRequest struct {
	Id   string
	Name string
}

type DeleteNinjaRequest struct {
	Id string
}

type GetDojoRequest struct {
	Id string
}

type CreateDojoRequest struct {
	Name string
}

type UpdateDojoRequest struct {
	Id   string
	Name string
}

type DeleteDojoRequest struct {
	Id string
}
