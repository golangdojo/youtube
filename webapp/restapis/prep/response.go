package main

type Status string

const (
	Success  Status = "SUCCESS"
	NotFound Status = "NOT_FOUND"
	Failure  Status = "FAILURE"
)

type GetNinjaResponse struct {
	Status Status
	Ninjas Ninja
}

type CreateNinjaResponse struct {
	Status Status
	Ninja  Ninja
}

type UpdateNinjaResponse struct {
	Status Status
	Ninja  Ninja
}

type DeleteNinjaResponse struct {
	Status Status
	Ninja  Ninja
}

type GetDojoResponse struct {
	Status Status
	Dojo   Dojo
}

type CreateDojoResponse struct {
	Status Status
	Dojo   Dojo
}

type UpdateDojoResponse struct {
	Status Status
	Dojo   Dojo
}

type DeleteDojoResponse struct {
	Status Status
	Dojo   Dojo
}
