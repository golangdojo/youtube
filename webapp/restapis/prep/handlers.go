package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

var DojoHandlers = DojoApiHandlers{}

type DojoApiHandlers struct{}

func (DojoApiHandlers) get(w http.ResponseWriter, r *http.Request) {
}

func (DojoApiHandlers) post(w http.ResponseWriter, r *http.Request) {
}

func (DojoApiHandlers) put(w http.ResponseWriter, r *http.Request) {
}

func (DojoApiHandlers) delete(w http.ResponseWriter, r *http.Request) {
}

var NinjaHandlers = NinjaApiHandlers{}

type NinjaApiHandlers struct{}

func (NinjaApiHandlers) get(w http.ResponseWriter, r *http.Request) {
	var getNinjaRequest GetNinjaRequest
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&getNinjaRequest)
	if err != nil {
		fmt.Println("Error decoding request body:", err)
		return
	}

	ninja, ok := ninjaDB.Get(getNinjaRequest.Id)
	if !ok {
		w.WriteHeader(http.StatusOK)
		getNinjaResponse := GetNinjaResponse{}
		getNinjaResponse.Status = NotFound
		_, err = fmt.Fprint(w, getNinjaResponse)
		if err != nil {
			fmt.Println("Error writing response ninja:", err)
		}
		return
	}
	getNinjaResponse := GetNinjaResponse{
		Success,
		ninja,
	}
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprint(w, getNinjaResponse)
	if err != nil {
		fmt.Println("Error writing response ninja:", err)
		return
	}
}

func (NinjaApiHandlers) post(w http.ResponseWriter, r *http.Request) {
	var createNinjaRequest CreateNinjaRequest
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&createNinjaRequest)
	if err != nil {
		fmt.Println("Error decoding request body:", err)
		return
	}
	ninja := ninjaDB.New(createNinjaRequest.Name)
	createNinjaResponse := CreateNinjaResponse{
		Success,
		ninja,
	}
	w.WriteHeader(http.StatusCreated)
	_, err = fmt.Fprint(w, createNinjaResponse)
	if err != nil {
		fmt.Println("Error writing response ninja:", err)
		return
	}
}

func (NinjaApiHandlers) put(w http.ResponseWriter, r *http.Request) {
	var updateNinjaRequest UpdateNinjaRequest
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&updateNinjaRequest)
	if err != nil {
		fmt.Println("Error decoding request body:", err)
		return
	}

	ninja, ok := ninjaDB.Get(updateNinjaRequest.Id)
	if !ok {
		fmt.Println("Didn't find ninja", err)
		w.WriteHeader(http.StatusNotFound)
		updateNinjaResponse := UpdateNinjaResponse{}
		updateNinjaResponse.Status = Failure
		fmt.Fprint(w, updateNinjaResponse)
		return
	}
	ninja.Name = updateNinjaRequest.Name
	updatedNinja, ok := ninjaDB.Update(ninja)
	if !ok {
		fmt.Println("Didn't update ninja:", updateNinjaRequest)
		w.WriteHeader(http.StatusNotFound)
		updateNinjaResponse := UpdateNinjaResponse{}
		updateNinjaResponse.Status = Failure
		fmt.Fprint(w, updateNinjaResponse)
		return
	}

	updateNinjaResponse := UpdateNinjaResponse{
		Success,
		updatedNinja,
	}
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprint(w, updateNinjaResponse)
	if err != nil {
		fmt.Println("Error writing response ninja:", err)
		return
	}
}

func (NinjaApiHandlers) delete(w http.ResponseWriter, r *http.Request) {
	var deleteNinjaRequest DeleteNinjaRequest
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&deleteNinjaRequest)
	if err != nil {
		fmt.Println("Error decoding request body:", err)
		return
	}

	ninja, ok := ninjaDB.Delete(deleteNinjaRequest.Id)
	if !ok {
		fmt.Println("Didn't delete ninja with id:", deleteNinjaRequest.Id)
		w.WriteHeader(http.StatusNotFound)
		deleteNinjaResponse := DeleteNinjaResponse{}
		deleteNinjaResponse.Status = Failure
		fmt.Fprint(w, deleteNinjaResponse)
		return
	}

	deleteNinjaResponse := DeleteNinjaResponse{
		Success,
		ninja,
	}
	w.WriteHeader(http.StatusOK)
	_, err = fmt.Fprint(w, deleteNinjaResponse)
	if err != nil {
		fmt.Println("Error writing response ninja:", err)
		return
	}
}
