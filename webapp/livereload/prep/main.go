package main

import (
	"fmt"
	"html/template"
	"net/http"
)

func handleFunc(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello World. It's me")
}

func templating(w http.ResponseWriter, r *http.Request) {
	var fileName = "helloWorld.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, nil)
}

func main() {
	http.HandleFunc("/", handleFunc)
	http.HandleFunc("/templating", templating)
	http.ListenAndServe(":3002", nil)
}
