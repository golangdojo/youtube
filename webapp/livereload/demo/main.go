package main

import (
	"html/template"
	"net/http"
)

func handleFunc(w http.ResponseWriter, r *http.Request) {
	var fileName = "helloWorld.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, nil)
}

func main() {
	http.HandleFunc("/", handleFunc)
	http.ListenAndServe(":3001", nil)
}
