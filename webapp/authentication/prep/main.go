package main

import (
	"golangdojo.com/golangdojo/authentication/users"
	"html/template"
	"net/http"
)

func userHandler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/sign_in_form":
		getSignInPage(w, r)
	case "/sign_up_form":
		getSignUpPage(w, r)
	case "/sign_in":
		signInUser(w, r)
	case "/sign_up":
		signUpUser(w, r)
	}
}

func getSignInPage(w http.ResponseWriter, r *http.Request) {
	fileName := "sign_in.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, nil)
}

func signInUser(w http.ResponseWriter, r *http.Request) {
	user := users.User{
		Email: r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	err := users.DefaultUserService.SignIn(user)
	if err != nil {
		fileName := "sign_in.html"
		t, _ := template.ParseFiles(fileName)
		t.ExecuteTemplate(w, fileName, "Credential not found.")
		return
	}
	fileName := "sign_in.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, "Signed in.")
	return
}

func getSignUpPage(w http.ResponseWriter, r *http.Request) {
	fileName := "sign_up.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, nil)
}

func signUpUser(w http.ResponseWriter, r *http.Request) {
	user := users.User{
		Email: r.FormValue("email"),
		Password: r.FormValue("password"),
	}

	err := users.DefaultUserService.SignUp(user)
	if err != nil {
		fileName := "sign_up.html"
		t, _ := template.ParseFiles(fileName)
		t.ExecuteTemplate(w, fileName, err.Error())
		return
	}
	fileName := "sign_up.html"
	t, _ := template.ParseFiles(fileName)
	t.ExecuteTemplate(w, fileName, "User signed up")
	return

}

func main() {
	http.HandleFunc("/", userHandler)
	http.ListenAndServe(":3001", nil)
}
