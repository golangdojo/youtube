package users

import (
	"errors"
	"fmt"
	"golang.org/x/crypto/bcrypt"
)

type User struct {
	Email    string `json:"email" validate:"required,email"`
	Password string `json:"password" validate:"required,max=100"`
}

var DefaultUserService userService

type userService struct {
}

func (userService) SignUp(u User) error {
	err := defaultAuthService.create(u)
	if err != nil {
		return err
	}
	return nil
}

func (userService) SignIn(u User) error {
	_, ok := defaultAuthService.get(u)
	if !ok {
		return errors.New("user not found")
	}
	return nil
}

var defaultAuthService = authService{
	authUsersByEmail: map[string]authUser{},
}

type authService struct {
	authUsersByEmail map[string]authUser
}

func (as *authService) create(user User) error {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 0)
	if err != nil {
		return err
	}
	au := authUser{user.Email, string(hash)}
	as.authUsersByEmail[user.Email] = au
	for _, authUser := range as.authUsersByEmail {
		fmt.Println(authUser.Email, authUser.PasswordHash)
	}
	return nil
}

func (as authService) get(user User) (authUser, bool) {
	hash, err := bcrypt.GenerateFromPassword([]byte(user.Password), 0)
	if err != nil {
		return authUser{}, false
	}
	au, ok := as.authUsersByEmail[user.Email]
	if !ok || string(hash) == au.PasswordHash {
		return authUser{}, false
	}
	return au, true
}

type authUser struct {
	Email        string
	PasswordHash string
}

func (userService) VerifyUser(user User) bool {
	authUser, ok := authUserDB[user.Email]
	if !ok {
		return false
	}
	err := bcrypt.CompareHashAndPassword(
		[]byte(authUser.passwordHash),
		[]byte(user.Password))
	return err == nil
}
