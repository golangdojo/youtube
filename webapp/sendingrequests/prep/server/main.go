package main

import (
	"encoding/json"
	"fmt"
	"net/http"
)

type Ninja struct {
	Name     string `json:"name"`
}

func url(w http.ResponseWriter, r *http.Request) {
	fmt.Println("url")
	name := r.FormValue("name")
	wallace := Ninja{name}
	fmt.Println(wallace)
	wallaceJson, err := json.Marshal(wallace)
	if err != nil {
		fmt.Println("Error when marshalling json", err)
	}
	w.Write(wallaceJson)
}

func body(w http.ResponseWriter, r *http.Request) {
	fmt.Println("body")
	switch r.Header.Get("Content-Type") {
	case "application/json":
		var wallace Ninja
		decoder := json.NewDecoder(r.Body)
		decoder.DisallowUnknownFields()
		err := decoder.Decode(&wallace)
		if err != nil {
			fmt.Println("Error when decoding json", err)
		} else {
			fmt.Println(wallace)
		}
		wallaceJson, err := json.Marshal(wallace)
		if err != nil {
			fmt.Println("Error when marshalling json", err)
		}
		w.Write(wallaceJson)
	default:
		fmt.Println("Not supported content type", r.Header.Get("Content-Type"))
	}
}

func handleFunc(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/url":
		url(w, r)
	case "/body":
		body(w, r)
	default:
		w.Write([]byte("Hello World"))
	}
}

func main() {
	http.HandleFunc("/", handleFunc)
	http.ListenAndServe("", nil)
}
