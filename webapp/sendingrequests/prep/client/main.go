package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
)

type Ninja struct {
	Name string `json:"name"`
}

func main() {
	// Plain URL
	response, error := http.Get("http://localhost")
	if error != nil {
		fmt.Printf("Failed to get http://localhost/, %s\n", error)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))
	}

	// URL key-value form
	response, error = http.PostForm(
		"http://localhost/url",
		url.Values{"name": {"wallace"}})
	if error != nil {
		fmt.Printf("Failed to post form http://localhost/, %s\n", error)
	} else {
		data, _ := ioutil.ReadAll(response.Body)
		fmt.Println(string(data))
	}

	// Request body
	wallace := Ninja{"Wallace"}
	wallaceJson, _ := json.Marshal(wallace)
	response, _ = http.Post(
		"http://localhost/body",
		"application/json",
		bytes.NewBuffer(wallaceJson))
	data, _ := ioutil.ReadAll(response.Body)
	fmt.Println(string(data))

	// NewRequest & Client
	// More scalable coding style with standalone requests
	wallace = Ninja{"Wallace"}
	wallaceJson, _ = json.Marshal(wallace)
	client := http.Client{}
	request, _ := http.NewRequest(
		"GET",
		"http://localhost/body",
		bytes.NewBuffer(wallaceJson))
	request.Header.Set("Content-Type", "application/json")
	response, _ = client.Do(request)
	//request2, _ := http.NewRequest(
	//	"GET",
	//	"http://localhost/body",
	//	bytes.NewBuffer(wallaceJson))
	//request2.Header.Set("Content-Type", "application/json")
	//response, _ = client.Do(request2)
	data, _ = ioutil.ReadAll(response.Body)
	fmt.Println(string(data))
}
