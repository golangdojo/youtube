package main

import (
	"fmt"
	"github.com/gorilla/sessions"
	"html/template"
	"math/rand"
	"net/http"
	"time"
)

// https://www.youtube.com/watch?v=jibxRSllvvE
// Note: Don't store your key in your source code. Pass it via an
// environmental variable, or flag (or both), and don't accidentally commit it
// alongside your code. Ensure your key is sufficiently random - i.e. use Go's
// crypto/rand or securecookie.GenerateRandomKey(32) and persist the result.
// var store = sessions.NewCookieStore([]byte(os.Getenv("SESSION_KEY")))
var store = sessions.NewCookieStore([]byte("random-secret"))
var userData = map[string]string {
	"Wallace": "badPassword",
}
var targets = []string{"Tommy", "Johnny", "Bobby"}
var templatePath = "C:/Users/Wallace/workspace/src/golangdojo.com/golangdojo/youtube/webapp/cookiesandsessions/prep/server/login.html"

func secretMission(w http.ResponseWriter, r *http.Request) {
	fmt.Println("*****secretMission*****")
	session, _ := store.Get(r, "session")
	_, ok := session.Values["ninjaId"]
	if !ok {
		displayLoginHtml(w, "Please login first")
		return
	}
	fmt.Fprint(w, "Your next target is: " + nextTarget())
}

func ninjaLogin(w http.ResponseWriter, r *http.Request) {
	fmt.Println("*****ninjaLogin*****")
	displayLoginHtml(w, "Please login")
}

func ninjaLoginSubmit(w http.ResponseWriter, r *http.Request) {
	fmt.Println("*****ninjaLoginSubmit*****")
	ninjaId := r.FormValue("ninjaId")
	password := r.FormValue("password")

	if matchingNinjaLogin(ninjaId, password) {
		displayLoginHtml(w, "Failed to log in!")
		return
	}

	session, _ := store.Get(r, "session")
	session.Values["ninjaId"] = ninjaId
	session.Save(r, w)
	displayLoginHtml(w, "You're logged in!")
	//http.Cookie
}

func matchingNinjaLogin(ninjaId string, password string) bool {
	// should store as hash
	return userData[ninjaId] != password
}

func ninjaLogout(w http.ResponseWriter, r *http.Request) {
	fmt.Println("*****ninjaLogout*****")
	session, _ := store.Get(r, "session")
	session.Options.MaxAge = -1
	session.Save(r, w)
	displayLoginHtml(w, "You've logged out.")
}

func displayLoginHtml(w http.ResponseWriter, header string) {
	t, err := template.ParseFiles("login.html")
	if err != nil {
		fmt.Println("Error reading html file", err)
	}
	t.ExecuteTemplate(w, "login.html", header)
}

func nextTarget() string {
	rand.Seed(time.Now().UnixNano())
	return targets[rand.Intn(len(targets))]
}

func handleFunction(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/mission":
		secretMission(w, r)
	case "/login":
		ninjaLogin(w, r)
	case "/login-submit":
		ninjaLoginSubmit(w, r)
	case "/logout":
		ninjaLogout(w, r)
	default:
		fmt.Fprint(w, "Hello Ninja")
	}
}

func main() {
	http.HandleFunc("/", handleFunction)
	http.ListenAndServe("", nil)
	//http.ListenAndServeTLS("", "cert.pem", "key.pem", nil)
	// leave a comment if you want a video on https
	// even though it's not technically quite directly related to Go 
	//go run $(go env GOROOT)/src/crypto/tls/generate_cert.go --host=localhost
}