package main

import (
	"strconv"
	"time"
)


var mission1 = Mission{
	"1635728208294634400",
	"Collect treasures",
	MissionStatusAvailable,
}
var mission2 = Mission{
		"1635728213865295100",
		"Pick up ninja stars",
	MissionStatusAvailable,
}
var missionDB = MissionDB{
	"1635728208294634400": mission1,
	"1635728213865295100": mission2,
}

type MissionDB map[string]Mission

type Mission struct {
	Id     string        `json:"id"`
	Name   string        `json:"name"`
	Status MissionStatus `json:"status"`
}

type MissionStatus string

const (
	MissionStatusAvailable = "AVAILABLE"
	MissionStatusActive    = "ACTIVE"
	MissionStatusCompleted = "COMPLETED"
)

func (MissionDB) Get(id string) (Mission, bool) {
	ninja, ok := missionDB[id]
	if !ok {
		return Mission{}, false
	}
	return ninja, true
}

func (MissionDB) GetAll() []Mission {
	var ninjas []Mission
	for _, ninja := range missionDB {
		ninjas = append(ninjas, ninja)
	}
	return ninjas
}

func (MissionDB) New(mission Mission) Mission {
	var id string
	for {
		id = strconv.FormatInt(time.Now().UnixNano(), 10)
		_, ok := missionDB.Get(id)
		if !ok {
			break
		}
	}
	mission.Id = id
	missionDB[id] = mission
	return mission
}

func (MissionDB) Update(mission Mission) (Mission, bool) {
	_, ok := missionDB.Get(mission.Id)
	if !ok {
		return Mission{}, false
	}
	missionDB[mission.Id] = mission
	return mission, true
}

func (MissionDB) Delete(id string) (Mission, bool) {
	ninja, ok := missionDB.Get(id)
	if !ok {
		return Mission{}, false
	}

	delete(missionDB, id)
	return ninja, true
}
