package main

import (
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)


func baseHandler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/ninja":
		switch r.Method {
		case "GET":
			missionHandlers.getMission(w, r)
		case "POST":
			missionHandlers.createMission(w, r)
		case "PUT":
			missionHandlers.updateMission(w, r)
		case "DELETE":
			missionHandlers.deleteMission(w, r)
		}
	default:
		errMsg := "Not supported url"
		fmt.Println(errMsg)
		fmt.Fprint(w, errMsg)
	}
}

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/missions", missionHandlers.getMissions).Methods("GET")
	router.HandleFunc("/missions/{id}", missionHandlers.getMission).Methods("GET")
	router.HandleFunc("/missions", missionHandlers.createMission).Methods("POST")
	router.HandleFunc("/missions", missionHandlers.updateMission).Methods("PUT")
	router.HandleFunc("/missions/{id}", missionHandlers.deleteMission).Methods("DELETE")
	http.ListenAndServe("", router)
}
