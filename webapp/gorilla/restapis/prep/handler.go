package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"net/http"
)

var missionHandlers = MissionHandlers{}

type MissionHandlers struct{}

func (MissionHandlers) getMission(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	missionId, ok := params["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error reading request when getMission", params)
		return
	}
	mission, ok := missionDB.Get(missionId)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Println("Didn't find mission when getMission", missionId)
		return
	}
	err := json.NewEncoder(w).Encode(mission)
	if err != nil {
		fmt.Println("Error encoding response when getMission", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (MissionHandlers) getMissions(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var missions []Mission
	for _, mission := range missionDB {
		missions = append(missions, mission)
	}
	err := json.NewEncoder(w).Encode(missions)
	if err != nil {
		fmt.Println("Error encoding response when getMission", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}

func (MissionHandlers) createMission(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var mission Mission
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&mission)
	if err != nil {
		fmt.Println("Error decoding request body when createMission:", err)
		return
	}
	mission = missionDB.New(mission)
	w.WriteHeader(http.StatusCreated)
	err = json.NewEncoder(w).Encode(mission)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println("Error encoding request body when createMission:", err)
		return
	}
}

func (MissionHandlers) updateMission(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	var mission Mission
	var decoder = json.NewDecoder(r.Body)
	decoder.DisallowUnknownFields()
	err := decoder.Decode(&mission)
	if err != nil {
		fmt.Println("Error decoding request body when createMission:", err)
		return
	}
	mission, ok := missionDB.Update(mission)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Println("Didn't to find mission when updateMission:", mission)
	}
	w.WriteHeader(http.StatusOK)
	err = json.NewEncoder(w).Encode(mission)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println("Error encoding request body when updateMission:", err)
		return
	}
}

func (MissionHandlers) deleteMission(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	params := mux.Vars(r)
	missionId, ok := params["id"]
	if !ok {
		w.WriteHeader(http.StatusBadRequest)
		fmt.Println("Error reading request when getMission", params)
		return
	}
	mission, ok := missionDB.Delete(missionId)
	if !ok {
		w.WriteHeader(http.StatusNotFound)
		fmt.Println("Didn't find mission when deleteMission", missionId)
		return
	}
	err := json.NewEncoder(w).Encode(mission)
	if err != nil {
		fmt.Println("Error encoding response when deleteMission", err)
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
}
