package main

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type RequestIdMissingError struct {
}

func (e RequestIdMissingError) Error() string {
	return fmt.Sprintf("Request ID is missing")
}

type WeaponError struct {
	errMessage string
}

func (err WeaponError) Error() string {
	return err.errMessage
}

func (err WeaponError) UnWrap() error {
	return errors.New("base error")
}

type NinjaError struct {
	errMessage string
}

func (err NinjaError) Error() string {
	return "ninjaError=" + err.errMessage
}

func (err NinjaError) UnWrap() error {
	return errors.New(err.errMessage)
}

type DojoError struct {
	ninjaErr   NinjaError
	errMessage string
}

func (err DojoError) Error() string {
	return "dojoError=" + err.errMessage + err.ninjaErr.Error()
}

func (err DojoError) UnWrap() error {
	return err.ninjaErr
}

var requestIdMissingErr = errors.New("requestId in query parameter is missing")

func readAFile() {
	_, err := os.Open("file.csv")
	if err != nil {
		fmt.Println("Failed to open file", err)
		return
	}
}

func setupADatabase() {
	const (
		host     = "localhost" // Default
		port     = 5432        // Default
		user     = "postgres"  // Default
		password = "postgres"  // Custom
		dbname   = "postgres"  // Default
	)
	connectionInfo := fmt.Sprintf(
		"host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		host, port, user, password, dbname)
	_, err := sql.Open("postgres", connectionInfo)
	if err != nil {
		fmt.Println("Failed to open SQL connection", err)
		return
	}
}

func spinUpAServer() {
	var handler http.ServeMux
	handler.HandleFunc("/", handleFunc)

	server := http.Server{
		Addr:         "", // localhost:80 (host:port)
		Handler:      &handler,
		ReadTimeout:  0,
		WriteTimeout: 0,
	}
	err := server.ListenAndServe()
	if err != nil {
		fmt.Println("Failed to kick off server", err)
		return
	}
}

func main() {
	// Intro
	//readAFile()
	//setupADatabase()
	//spinUpAServer()

	// Use errors.New()
	err := errors.New("error occurred")
	if err != nil {
		// Newbies sometimes wonder if there's a better way
		// Check out last video on proposals
	}

	// Use the error interface
	err = errors.New("basic error")
	fmt.Println(err.Error())
	fmt.Println(err)

	// Define custom error types as needed
	err = RequestIdMissingError{}

	// Don't ignore error
	_ = someUncertainOperation()
	err = someUncertainOperation()
	if err != nil {
		// Handle error
		// ...
	}

	// Descriptive error message
	err = errors.New("invalid input")
	err = errors.New("requestId in query parameter is missing")

	// Fatal functions to exit and append the error
	if err != nil {
		//log.Fatal("Unable to continue", err)
	}

	// Return errors as early as possible
	err = returnErrorsEarly()
	err = returnErrorsLate()
	// Define common errors
	err = handleRequest("")

	// Defer to properly clean up resources, even in the event of an error
	readFile("./file.csv")

	// errors.Is, errors.As, errors.UnWrap
	ninjaErr1 := NinjaError{errMessage: "ninja error"}
	ninjaErr2 := NinjaError{errMessage: "ninja error"}
	dojoErr1 := DojoError{ninjaErr: ninjaErr1}

	fmt.Println("is as unwrap")
	fmt.Println(errors.Is(ninjaErr1, dojoErr1))              // false
	fmt.Println(errors.Is(ninjaErr1, ninjaErr2))             // true
	fmt.Println(errors.As(wrapError(ninjaErr1), &ninjaErr1)) // true
	fmt.Println(errors.Unwrap(wrapError(ninjaErr1)))         // ninjaError=ninja error

	// net/http's Error function
	handleHttpRequest()

	// Propagate errors across API boundaries with context
	handleErrorsWithContext()
}

func wrapError(err error) error {
	return fmt.Errorf("wrapping error: %w", err)
}

func returnErrorsEarly() error {
	err := someUncertainOperation()

	if err != nil {
		fmt.Println("Something went wrong")
		return err
	}
	// Performant normal operations
	// ...
	return nil
}

func returnErrorsLate() error {
	err := someUncertainOperation()

	if err == nil {
		// Performant normal operations
		// ...
		return nil
	}
	fmt.Println("Something went wrong")
	return err
}

func someUncertainOperation() error {
	// Perform some operation incurring an error
	// ...
	return errors.New("error")
}

func handleRequest(requestId string) error {
	if len(requestId) == 0 {
		return requestIdMissingErr
	}
	// Handle request
	return nil
}

func readFile(src string) error {
	in, err := os.Open(src)
	if err != nil {
		return err
	}
	// Verses in.Close() at the end, which may not be called
	defer in.Close()

	var content []byte
	_, err = in.Read(content)
	if err != nil {
		return err
	}

	fmt.Println(content)

	return nil
}

func handleHttpRequest() {
	var handler http.ServeMux
	handler.HandleFunc("/", handleFunc)

	server := http.Server{
		Addr:         "", // localhost:80 (host:port)
		Handler:      &handler,
		ReadTimeout:  0,
		WriteTimeout: 0,
	}
	server.ListenAndServe()
}

func handleFunc(w http.ResponseWriter, r *http.Request) {
	if err := validate(r); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
	}
}

func validate(r *http.Request) error {
	return requestIdMissingErr
}

func handleErrorsWithContext() {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "requestId", "123")

	go someApiCall(ctx)
	time.Sleep(1)
}

func someApiCall(ctx context.Context) {
	err := someUncertainOperation()
	if err != nil {
		log.Fatalf("Failed for requestId={%s} and error={%s}",
			ctx.Value("requestId"), err)
	}
}
