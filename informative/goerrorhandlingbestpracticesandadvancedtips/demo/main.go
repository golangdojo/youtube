package main

import (
	"context"
	"errors"
	"log"
	"time"
)

func main() {
	ctx := context.Background()
	ctx = context.WithValue(ctx, "requestId", "123")
	go someApiCall(ctx)
	time.Sleep(1)
}

func someApiCall(ctx context.Context) {
	err := someUncertainOperation()
	if err != nil {
		log.Fatalf("Failed for requestId={%s} and error={%s}",
			ctx.Value("requestId"), err)
	}
}

func someUncertainOperation() error {
	return errors.New("error occurred")
}
