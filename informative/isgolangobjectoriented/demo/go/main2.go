package main

import . "fmt"

type RegularHuman interface {
	chill()
}

type Ninja struct {
}

func (Ninja) chill() {
	Println("Chilling as a ninja")
}

