class Example {

    class RegularHuman {
        public void chill() {
            System.out.println("Chilling...");
        }
    }

    class Ninja extends RegularHuman {
        public void attack() {
            System.out.println("Throwing ninja stars");
        }
    }

    class SeniorNinja extends Ninja {
    }
}