class Example {

    static class RegularHuman {
        public void chill() {
            System.out.println("Chilling...");
        }
    }

    static class Ninja extends RegularHuman {
        public void attack() {
            System.out.println("Throwing ninja stars");
        }
    }

    static class SeniorNinja extends Ninja {
        public void attack() {
            super.attack();
            System.out.println("Swinging ninja swords");
            chill();
        }
    }

    public static void main(String[] args) {
        SeniorNinja sn = new SeniorNinja();
        sn.attack();
    }
}