package main

type intQueue []int

func (q *intQueue) intEnqueue(value int) {
	*q = append(*q, value)
}

func (q *intQueue) intDequeue() int {
	if len(*q) == 0 {
		var zero int
		return zero
	}
	r := (*q)[0]
	*q = (*q)[1:]
	return r
}

func intmain() {
	stack := intQueue{}
	stack.intEnqueue(1)
	stack.intEnqueue(2)

	println(stack.intDequeue())
	println(stack.intDequeue())
}

/*






*/