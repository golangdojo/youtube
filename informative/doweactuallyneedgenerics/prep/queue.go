package main

type queue []interface{}

func (q *queue) enqueue(value interface{}) {
	*q = append(*q, value)
}

func (q *queue) dequeue() (interface{}, bool) {
	if len(*q) == 0 {
		var zero interface{}
		return zero, false
	}
	r := (*q)[0]
	*q = (*q)[1:]
	return r, false
}

func main() {
	queue := queue{}
	queue.enqueue("string1")
	queue.enqueue(1)

	d1, _ := queue.dequeue()
	d2, _ := queue.dequeue()
	println(d1.(string))
	println(d2.(int))
}

/*


Golang Generics - Past vs Future

Thumbnail -
https://external-preview.redd.it/IGY803KOV9nYnchRdJoWXW1UpwKuCzQx5KAIoVY2DLE.jpg?s=e0e1ee1508ab5db74f9f6fa33831d2089205b438


One of the most greatly debated topics in the Golang community is the lack of generics for the language

There's been proposals and now design drafts to add Generics to the language. However, if you want to use generics now and implement something as close as possible, it's hard to find beginner friendly tutorials on YouTube for that. At least, I tried and I couldn't find any.

So in today's video, I will show you 3 things
1) quick and simple alternatives to generics with the current version of Go
2) how to implement generics in go right now because you can't wait for future versions of Go, if they would ever come with generics that is
3) how to actually use generics in the future with the design currently being proposed. With the help of the golang playground

What's up ninjas





alternatively you may use
array
slice
map



hacky ways
interface

there are also other workarounds like reflection that are even less intuitive
if you want to learn how to learn more about things like reflection or other tips & tricks in go programming, sub

https://appliedgo.net/generics/



If you know of better ways to implement generics in the current version of Go. Make sure to leave a comment down below. With that let's chat again in our next video



*/
