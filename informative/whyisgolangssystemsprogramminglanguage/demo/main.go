package main

import (
	"fmt"
	"os"
)

func main() {
	pwd, _ := os.Getwd()
	info, _ := os.Stat(pwd + "/sessions.go")
	mode := info.Mode()
	fmt.Println(mode)
}
