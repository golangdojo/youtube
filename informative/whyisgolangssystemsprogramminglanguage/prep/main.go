package main

import (
	"fmt"
	"os"
)

func main() {
	// file permission management
	pwd, _ := os.Getwd()
	info, _ := os.Stat(pwd)
	mode := info.Mode()
	fmt.Println(mode)
}
