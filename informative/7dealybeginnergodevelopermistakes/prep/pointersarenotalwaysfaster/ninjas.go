package main

import (
	"github.com/google/uuid"
)

type Ninja struct {
	ID             uuid.UUID
	Name           string
	Age            int
	IsSensei       bool
	Experience     int
	Level          string
	MainWeapon     string
	Origin         string
	CurrentMission string
	Dojo           string
	Greeting       string
}

func buildNinja() Ninja {
	n := Ninja{
		ID:             uuid.New(),
		Name:           "Tommy",
		Experience:     312,
		MainWeapon:     "Ninja Star",
		CurrentMission: "Secret Mission 1",
		Origin:         "Wonderland",
		Dojo:           "Golang Dojo",
		Greeting:       "你好",
	}
	return n
}

func returnNinjaByValue(previous Ninja) Ninja {
	current := Ninja{
		ID:             previous.ID,
		Name:           previous.Name,
		Experience:     previous.Experience,
		MainWeapon:     previous.MainWeapon,
		CurrentMission: previous.CurrentMission,
		Origin:         previous.Origin,
		Dojo:           previous.Dojo,
		Greeting:       previous.Greeting,
	}
	return current
}

func returnNinjaByPointer(previous *Ninja) *Ninja {
	current := Ninja{
		ID:             previous.ID,
		Name:           previous.Name,
		Experience:     previous.Experience,
		MainWeapon:     previous.MainWeapon,
		CurrentMission: previous.CurrentMission,
		Origin:         previous.Origin,
		Dojo:           previous.Dojo,
		Greeting:       previous.Greeting,
	}
	return &current
}

func main() {

}

func passNinjaByValue(n Ninja) {
	// Doing ninja stuff...
}

func passNinjaByPointer(n *Ninja) {
	// Doing ninja stuff...
}
