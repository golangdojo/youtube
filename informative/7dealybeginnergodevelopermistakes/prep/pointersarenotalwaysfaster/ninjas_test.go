package main

import (
	"fmt"
	"testing"
)

var tommy Ninja

func BenchmarkReturnNinjaByValue(b *testing.B) {
	t := buildNinja()
	for i := 0; i < b.N; i++ {
		t = returnNinjaByValue(t)
	}
	tommy = t
	fmt.Println(tommy)
}

var jonny *Ninja

func BenchmarkReturnNinjaByPointer(b *testing.B) {
	j := buildNinja()
	jPointer := &j
	for i := 0; i < b.N; i++ {
		jPointer = returnNinjaByPointer(jPointer)
	}
	jonny = jPointer
	fmt.Println(*jonny)
}
