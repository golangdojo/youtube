package main

import (
	"errors"
	"fmt"
)

func main() {
	// What if you were to add num3?
	if err := checkEvenAndNegative(1, -2); err != nil {
		fmt.Println("Failed even and negative check")
		return
	}
	fmt.Println("Passed even and negative check")
}

func checkEvenAndNegative(num1 int, num2 int) error {
	if err := checkEven(num1); err != nil {
		if err := checkNegative(num1); err != nil {
			if err := checkEven(num2); err != nil {
				if err := checkNegative(num2); err != nil {
					return nil
				} else {
					return fmt.Errorf("num2=%v failed=%v", num1, err)
				}
			} else {
				return fmt.Errorf("num2=%v failed=%v", num1, err)
			}
		} else {
			return fmt.Errorf("num1=%v failed=%v", num1, err)
		}
	} else {
		return fmt.Errorf("num1=%v failed=%v", num1, err)
	}
}

func checkEvenAndNegativeWithEarlyReturn(num1 int, num2 int) error {
	if err := checkEven(num1); err != nil {
		return fmt.Errorf("num1=%v failed=%v", num1, err)
	}
	if err := checkNegative(num1); err != nil {
		return fmt.Errorf("num1=%v failed=%v", num1, err)
	}
	if err := checkEven(num2); err != nil {
		return fmt.Errorf("num2=%v failed=%v", num1, err)
	}
	if err := checkNegative(num2); err != nil {
		return fmt.Errorf("num2=%v failed=%v", num1, err)
	}
	return nil
}

func checkEven(num int) error {
	if num%2 != 0 {
		return errors.New("non even number")
	}
	return nil
}

func checkNegative(num int) error {
	if num >= 0 {
		return errors.New("non negative number")
	}
	return nil
}
