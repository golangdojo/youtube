package main

import (
	"fmt"
	"time"
)

func main() {
	s := []int{1, 2, 3}
	for _, i := range s {
		go func() {
			fmt.Println(i)
		}()
	}
	time.Sleep(time.Second)

	for _, i := range s {
		localI := i
		go func() {
			fmt.Println(localI)
		}()
	}
	time.Sleep(time.Second)

	for _, i := range s {
		go func(localI int) {
			fmt.Println(localI)
		}(i)
	}
	time.Sleep(time.Second)
}
