package main

import (
	"fmt"
)

func main() {
	breakSignal, exitSignal := make(chan struct{}), make(chan struct{})
	//go catchSignalsWithLabel(breakSignal, exitSignal)
	go catchSignalsWithBreak(breakSignal, exitSignal)
	breakSignal <- struct{}{}
	<-exitSignal
	fmt.Println("Exiting...")
}

func catchSignalsWithBreak(breakSignal chan struct{}, exitSignal chan struct{}) {
	for {
		fmt.Println("Waiting for signal...")
		select {
		case <-breakSignal:
			fmt.Println("Caught signal...")
			break
			// A "break" statement terminates execution of
			// the innermost "for" , "switch" , or "select"
			// statement within the same function.
		}
	}
	exitSignal <- struct{}{} // Unreachable code
}

func catchSignalsWithLabel(breakSignal chan struct{}, exitSignal chan struct{}) {
loop:
	for {
		fmt.Println("Waiting for signal...")
		select {
		case <-breakSignal:
			fmt.Println("Caught signal...")
			break loop
		}
	}
	exitSignal <- struct{}{}
}
