package main

import "fmt"

func main() {

}

func mapValueIsNotAddressable() {
	var m map[int]string = make(map[int]string)
	m[1] = "Golang Dojo"
	var sp *string = &m[1] // Compiler error, not addressable
	fmt.Println(*sp)
}

func mapValueIsNotAddressableStill() {
	var m map[int]string = make(map[int]string)
	m[1] = "Golang Dojo"
	s := m[1]           // First, map value to string variable
	var sp *string = &s // OK, string variable is addressable
	fmt.Println(*sp)
}

func sliceValueIsNotAddressable() {
	var s []string
	s = append(s, "Golang Dojo")
	var sp *string = &s[0]
	fmt.Println(*sp)
}

type Ninja struct {
	Experience int
}

func (n *Ninja) DoMission() {
	n.Experience++
}

func (n Ninja) Greet() {
	fmt.Println("I'm a Ninja")
}

type MissionDoer interface {
	DoMission()
}

type Greeter interface {
	Greet()
}

func DoMissions(dm MissionDoer, numMissions int) {
	for i := 0; i < numMissions; i++ {
		dm.DoMission()
	}
}

func DoGreetings(g Greeter, numGreetings int) {
	for i := 0; i < numGreetings; i++ {
		g.Greet()
	}
}

func interfaceIsNotAddressable() {
	// A bare value
	var tommy Ninja
	// INVALID: tommy#DoMission has a value receiver
	DoMissions(tommy, 3)
	// VALID: &tommy#DoMission has a pointer receiver
	DoMissions(&tommy, 3)
	// VALID: tommy#DoMission has a value receiver
	DoGreetings(tommy, 3)

	// A pointer value
	jonnyP := new(Ninja)
	// VALID: jonnyP#DoMission has a pointer receiver
	DoMissions(jonnyP, 3)
	// INVALID: *jonnyP#DoMission has a value receiver
	DoMissions(*jonnyP, 3)
	// VALID: Ninja#Greet has a value receiver
	DoGreetings(jonnyP, 3)
}
