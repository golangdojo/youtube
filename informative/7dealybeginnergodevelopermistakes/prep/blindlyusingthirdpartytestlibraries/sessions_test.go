package main

import (
	"github.com/google/uuid"
	"github.com/stretchr/testify/assert"
	"testing"
)

// Package testing
func TestSessionsImpl(t *testing.T) {
	sessions := SessionsImpl{}
	expectedSessionCreated := uuid.New()
	actualSessionCreated := sessions.CreateSession(uuid.New())
	if expectedSessionCreated != actualSessionCreated {
		t.Fatal("Session created incorrectly")
	}

	expectedSessionChecked := true
	actualSessionChecked := sessions.CheckSession(expectedSessionCreated)
	if expectedSessionChecked != actualSessionChecked {
		t.Error("Session checked failed")
	}
}

// Testify
func TestSessionsImplWithAsserts(t *testing.T) {
	sessions := SessionsImpl{}
	expectedSessionCreated := uuid.New()
	actualSessionCreated := sessions.CreateSession(uuid.New())
	assert.Equal(t, expectedSessionCreated, actualSessionCreated)

	expectedSessionChecked := true
	actualSessionChecked := sessions.CheckSession(expectedSessionCreated)
	assert.Equal(t, expectedSessionChecked, actualSessionChecked)
}
