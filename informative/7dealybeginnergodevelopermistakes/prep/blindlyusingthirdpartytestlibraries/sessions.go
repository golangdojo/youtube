package main

import "github.com/google/uuid"

type Sessions interface {
	CreateSession(userUuid uuid.UUID) uuid.UUID
	CheckSession(token uuid.UUID) bool
	DeleteSession(token uuid.UUID) error
}

type SessionsImpl struct {
	// Session persistence...
}

func (s SessionsImpl) CreateSession(userUuid uuid.UUID) uuid.UUID {
	// Create session...
	return uuid.New()
}

func (s SessionsImpl) CheckSession(token uuid.UUID) bool {
	// Check session...
	return true
}

func (s SessionsImpl) DeleteSession(token uuid.UUID) error {
	// Delete session...
	return nil
}
