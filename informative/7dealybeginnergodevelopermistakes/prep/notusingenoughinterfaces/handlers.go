package main

import "net/http"

type MainHandler struct {
	S Sessions // Interface type, not struct type
}

func (mh MainHandler) handle(w http.ResponseWriter, r *http.Request) {
	// Handle request...
}
