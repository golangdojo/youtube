package main

import (
	"fmt"
	"github.com/google/uuid"
)

type DiffSessionsImpl struct {
}

func (d DiffSessionsImpl) CreateSession(userUuid uuid.UUID) uuid.UUID {
	// Create session...
}

func (d DiffSessionsImpl) CheckSession(token uuid.UUID) bool {
	// Check session...
}

func (d DiffSessionsImpl) DeleteSession(token uuid.UUID) error {
	// Delete session...
}

func main() {
	var sessions SessionsImpl = SessionsImpl{}
	session := sessions.CreateSession(uuid.New())
	exists := sessions.CheckSession(session)
	if !exists {
		fmt.Println("Something went wrong")
		return
	}
	err := sessions.DeleteSession(session)
	if err != nil {
		fmt.Println("Something went wrong")
		return
	}
	checkUserSessionStruct(SessionsImpl{}, uuid.New())
	checkUserSessionInterface(SessionsImpl{}, uuid.New())
	checkUserSessionInterface(DiffSessionsImpl{}, uuid.New())
}

func checkUserSessionStruct(s SessionsImpl, userId uuid.UUID) bool {
	return s.CheckSession(userId)
}

func checkUserSessionInterface(s Sessions, userId uuid.UUID) bool {
	return s.CheckSession(userId)
}
