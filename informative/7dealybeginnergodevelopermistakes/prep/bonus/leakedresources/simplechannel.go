package main

import (
	"fmt"
	"time"
)

func main() {
	ch := make(chan int)
	go withoutCleanup(ch)

	fmt.Println("Wait a seconds for the other goroutine...")
	time.Sleep(time.Second)

	//done := make(chan struct{})
	//ch := make(chan int)
	//
	//go withCleanup(done, ch)
	//close(done)
	//
	//fmt.Println("Wait a seconds for the other goroutine...")
	//time.Sleep(time.Second)
}

func withoutCleanup(ch chan int) {
	x := <-ch // forever blocked on a nil channel
	fmt.Println("Without cleanup is done with:", x)
}

func withCleanup(done chan struct{}, ch chan int) {
	select {
	case x := <-ch: // forever blocked on a nil channel
		fmt.Println("Without cleanup is done with:", x)
	case <-done: // to listen to signal on this channel
		fmt.Println("Cleanup signal is here")
		return
	}
}
