package main

import (
	"context"
	"fmt"
	"github.com/google/uuid"
	"time"
)

func main() {
	//ctx := context.WithValue(context.Background(), "requestId", uuid.New().String())
	//go timeConsumingOperation(ctx)

	ctx := context.WithValue(context.Background(), "requestId", uuid.New().String())
	cancelCtx, cancel := context.WithCancel(ctx)
	go timeConsumingOperation(cancelCtx)
	cancel()
}

func timeConsumingOperation(ctx context.Context) {
	for {
		select {
		case <-ctx.Done():
			fmt.Println("Signaling done...")
			return
		default:
			time.Sleep(time.Second)
			fmt.Println("Doing some work...")
		}
	}
}
