package main

import (
	"sync"
)

var memPool = &sync.Pool{
	New: func() interface{} {
		mem := make([]byte, 1024)
		return &mem
	},
}

func withMemPool() {
	mem := memPool.Get()

	// Doing some random work
	work(mem)

	memPool.Put(mem)
}

func withoutMemPool() {
	mem := make([]byte, 1024)

	// Doing some random work
	work(mem)

	memPool.Put(mem)
}

func work(mem any) any {
	return mem
}
