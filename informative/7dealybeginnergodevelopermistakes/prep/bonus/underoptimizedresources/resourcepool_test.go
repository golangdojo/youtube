package main

import "testing"

func BenchmarkWithoutMemPool(b *testing.B) {
	for i := 0; i < b.N; i++ {
		withoutMemPool()
	}
}

func BenchmarkWithMemPool(b *testing.B) {
	for i := 0; i < b.N; i++ {
		withMemPool()
	}
}
