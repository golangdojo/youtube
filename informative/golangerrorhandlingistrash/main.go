package main

import (
	"errors"
	"fmt"
)

func main() {
	f, err := someF()
	if err != nil {
		// Handle error...
		return
	}
	fmt.Println(f)
	f, err = someOtherF()
	if err != nil {
		// Handle error again...
		return
	}
	fmt.Println(f)
}

func someF() (int, error) {
	// Some operations...
	return 0, someE()
}

func someOtherF() (int, error) {
	// Some operations...
	return 0, someE()
}

func someE() error {
	return errors.New("same type of error")
}
