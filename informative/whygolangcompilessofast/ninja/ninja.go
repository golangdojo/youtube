package ninja

import "golangdojo.com/golangdojo/youtube/informative/whygolangcompilessofast/dojo"

type Ninja struct {
	Name           string
	AssociatedDojo dojo.Dojo
}
