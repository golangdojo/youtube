package main

import (
	"fmt"
)

func main() {
	a := (*interface{})(nil)
	var b interface{} = (*interface{})(nil)
	fmt.Println(a == b, a == nil, b == nil)
}










func slice() {
	slice := []string{"string1", "string2", "string3"}
	for index, element := range slice {
		fmt.Println(index, element)
	}
}
