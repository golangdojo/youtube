module golangdojo.com/golangdojo/youtube/informative/golangdependencyinjectionisamess/relection

go 1.18

require github.com/google/uuid v1.3.0

require go.uber.org/dig v1.16.1 // indirect
