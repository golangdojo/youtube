package main

import (
	"errors"
	"github.com/google/uuid"
	"go.uber.org/dig"
	"log"
	"net/http"
)

func main() {
	c := dig.New()

	err := c.Provide(func() ([]Ninja, error) {
		return NewSampleNinjas(), errors.New("fail to receive []Ninja")
	})
	if err != nil {
		log.Println("Failed to construct []Ninja")
	}

	err = c.Provide(func(ninjas []Ninja) (NinjasDB, error) {
		return NewNinjasDB(ninjas), errors.New("fail to receive NinjasDB")
	})
	if err != nil {
		log.Println("Failed to construct Handlers")
	}

	err = c.Provide(func(ninjasDB NinjasDB) (Handlers, error) {
		return NewHandlers(ninjasDB), errors.New("fail to receive Handlers")
	})
	if err != nil {
		log.Println("Failed to construct Handlers")
	}

	var sampleNinjas []Ninja
	if err := c.Provide(sampleNinjas); err != nil {
		log.Println(err)
	}
	var ninjasDB NinjasDB
	if err := c.Provide(ninjasDB); err != nil {
		log.Println(err)
	}
	var handlers Handlers
	if err := c.Provide(handlers); err != nil {
		log.Println(err)
	}

	http.HandleFunc("/ninjas", handlers.Ninjas)
	http.ListenAndServe("", nil)
}

func NewSampleNinjas() []Ninja {
	return []Ninja{
		{
			Id:   uuid.New(),
			Name: "Tommy",
		},
	}
}
