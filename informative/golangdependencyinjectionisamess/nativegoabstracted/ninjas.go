package main

import "github.com/google/uuid"

var N = NinjasDB{
	ExistingNinjas: []Ninja{
		{
			Name: "Tommy",
			Id:   uuid.New(),
		},
	},
}

type NinjasDB struct {
	ExistingNinjas []Ninja
}

func (n *NinjasDB) SelectNinja(ninjaId uuid.UUID) (Ninja, bool) {
	for _, ninja := range n.ExistingNinjas {
		if ninja.Id == ninjaId {
			return ninja, true
		}
	}
	return Ninja{}, false
}

type Ninja struct {
	Id   uuid.UUID
	Name string
}
