module golangdojo.com/golangdojo/youtube/informative/golangdependencyinjectionisamess/gogenerate

go 1.18

require github.com/google/uuid v1.3.0

require github.com/google/wire v0.5.0
