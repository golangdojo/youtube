package main

import "github.com/google/uuid"

type NinjasDB struct {
	ExistingNinjas []Ninja
}

func NewNinjasDB(ninjas []Ninja) NinjasDB {
	return NinjasDB{
		ExistingNinjas: ninjas,
	}
}

func (n *NinjasDB) SelectNinja(ninjaId uuid.UUID) (Ninja, bool) {
	for _, ninja := range n.ExistingNinjas {
		if ninja.Id == ninjaId {
			return ninja, true
		}
	}
	return Ninja{}, false
}

type Ninja struct {
	Id   uuid.UUID
	Name string
}
