//go:build wireinject
// +build wireinject

package main

import (
	"github.com/google/uuid"
	"github.com/google/wire"
	"net/http"
)

func main() {
	/*
		Top of the file: `//+build wireinject`
		go install github.com/google/wire/cmd/wire@latest
		wire
	*/
	handlers := InitializeHandlers()

	http.HandleFunc("/ninjas", handlers.Ninjas)
	http.ListenAndServe("", nil)
}

func InitializeHandlers() Handlers {
	wire.Build(NewHandlers, NewNinjasDB, NewSampleNinjas)
	return Handlers{}
}

func NewSampleNinjas() []Ninja {
	return []Ninja{
		{
			Id:   uuid.New(),
			Name: "Tommy",
		},
	}
}
