package main

import (
	"github.com/google/uuid"
	"net/http"
)

func main() {
	sampleNinjas := NewSampleNinjas()
	ninjasDB := NewNinjasDB(sampleNinjas)
	handlers := NewHandlers(ninjasDB)
	// What if you want to add a logger in all these structs

	http.HandleFunc("/ninjas", handlers.Ninjas)
	http.ListenAndServe("", nil)
}

func NewSampleNinjas() []Ninja {
	return []Ninja{
		{
			Id:   uuid.New(),
			Name: "Tommy",
		},
	}
}
