package main

import (
	"github.com/google/uuid"
	"net/http"
)

type Handlers struct {
	NinjasDB NinjasDB
}

func NewHandlers(ninjaDB NinjasDB) Handlers {
	return Handlers{
		NinjasDB: ninjaDB,
	}
}

func (h *Handlers) Ninjas(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		h.CheckNinja(w, r)
	}
}

func (h *Handlers) CheckNinja(w http.ResponseWriter, r *http.Request) {
	ninjaIdString := r.FormValue("ninjaId")
	ninjaId, err := uuid.FromBytes([]byte(ninjaIdString))
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}

	_, exists := h.NinjasDB.SelectNinja(ninjaId)
	if !exists {
		w.WriteHeader(http.StatusNotFound)
		return
	}

	w.WriteHeader(http.StatusFound)
}
