package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	// goroutine
	go func() {
		fmt.Println("New goroutine")
	}()
	fmt.Println("This is the old one")

	// channel
	ch := make(chan string)
	go func() {
		ch <- "Hello World"
	}()
	fmt.Println(<-ch)
	ch = make(chan string, 2)
	ch <- "1"
	ch <- "2"
	fmt.Println(<-ch)
	fmt.Println(<-ch)

	// wait group
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		fmt.Println("New goroutine")
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("This is the old one")

	// mutex
	iterations := 1000
	sum := 0
	wg.Add(iterations)
	var mu sync.Mutex
	for i := 0; i < iterations; i++ {
		go func() {
			mu.Lock()
			sum++
			mu.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(sum)

	// once
	sum = 0
	wg.Add(iterations)
	var once sync.Once
	for i := 0; i < iterations; i++ {
		go func() {
			once.Do(func() {
				sum++
			})
		}()
		wg.Done()
	}
	wg.Wait()
	fmt.Println(sum)

	// pool
	memPool := &sync.Pool{
		New: func() interface{} {
			mem := make([]byte, 1024)
			return &mem
		},
	}
	mem := memPool.Get().(*[]byte)
	// ...
	memPool.Put(mem)

	// cond (signal & broadcast)
	c := sync.NewCond(&sync.Mutex{})
	go func() {
		c.L.Lock()
		// changing some condition
		c.L.Unlock()
		c.Signal()
		c.Broadcast()
	}()
	c.L.Lock()
	c.Wait()
	c.L.Unlock()

	// goroutine-safe map
	syncMap := sync.Map{}
	wg.Add(iterations)
	for i := 0; i < iterations; i++ {
		go func() {
			syncMap.Store(0, i)
			wg.Done()
		}()
	}
	wg.Wait()

	// atomic
	var i int64
	atomic.AddInt64(&i, 1)
	mu.Lock()
	i += 1
	mu.Unlock()
	var av atomic.Value
	type ninja struct {
		name string
	}
	av.Store(ninja{"Wallace"})
	av.Load()
}
