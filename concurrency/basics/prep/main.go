package main

import (
	"fmt"
	"sync"
	"sync/atomic"
)

func main() {
	// goroutine
	go func() {
		fmt.Println("New Goroutine")
	}()
	fmt.Println("This is the old one")

	// channel - uni-directional, buffered, iterating, select
	ch := make(chan string)
	go func() {
		ch <- "Hello World"
	}()
	fmt.Println(<-ch)
	ch = make(chan string, 2)
	ch <- "1"
	ch <- "2"
	fmt.Println(<-ch)
	fmt.Println(<-ch)
	ch <- "3"
	ch <- "4"
	close(ch)
	for msg := range ch {
		fmt.Println(msg)
	}
	ch = make(chan string, 2)
	//ch <- "5"
	select {
	case m := <-ch:
		fmt.Println(m)
	default:
		fmt.Println("Default!")
	}

	// wait group
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		fmt.Println("New Goroutine")
		wg.Done()
	}()
	wg.Wait()
	fmt.Println("This is the old one")

	// mutex
	iterations := 1000
	var sum int
	wg.Add(iterations)
	var mu sync.Mutex
	for i := 0; i < iterations; i++ {
		go func() {
			mu.Lock()
			sum++
			mu.Unlock()
			wg.Done()
		}()
	}
	wg.Wait()
	fmt.Println(sum)

	// once
	sum = 0
	var once sync.Once
	wg.Add(100)
	for i := 0; i < 100; i++ {
		go func() {
			once.Do(func() { sum++ })
		}()
		wg.Done()
	}
	wg.Wait()
	fmt.Println(sum)

	// resource pool
	memPool := &sync.Pool{
		New: func() interface{} {
			mem := make([]byte, 1024)
			return &mem
		},
	}
	mem := memPool.Get().(*[]byte)
	memPool.Put(mem)

	// signal & broadcast
	c := sync.NewCond(&sync.Mutex{})
	go func() {
		c.Signal()
		//c.Broadcast()
	}()
	c.L.Lock()
	c.Wait()
	c.L.Unlock()

	// goroutine-safe map
	syncMap := sync.Map{}
	//regularMap := make(map[int]interface{})
	wg.Add(100)
	for i := 0; i < 100; i++ {
		go func() {
			syncMap.Store(0, 0)
			//regularMap[0] = i
			wg.Done()
		}()
	}
	wg.Wait()

	// atomic
	var i int64
	atomic.AddInt64(&i, 1)
	mu.Lock()
	i += 1
	mu.Unlock()
	var av atomic.Value
	type ninja struct {
		name string
	}
	av.Store(ninja{"Wallace"})
	av.Load()
	av.Store(ninja{"Not Wallace"})

}
