package main

import (
	"fmt"
	"sync"
)

func main() {
	//problem()
	//fmt.Println("Attacked attempt succeeded")
	//fmt.Println("Attacked attempt succeeded")
	//basics()
	//multiple()
	moreProblems()
}

func problem() {
	go attackAttempt()
}

func attackAttempt() {
	fmt.Println("Attacked attempt succeeded")
}

func basics() {
	var beeper sync.WaitGroup
	beeper.Add(1)
	// passing by value won't work
	go attack(&beeper)
	beeper.Wait()
}

func attack(beeper *sync.WaitGroup) {
	defer beeper.Done()
	fmt.Println("Attacked!")
}

func multiple() {
	var beeper sync.WaitGroup
	targets := []string{
		"Tommy",
		"Jimmy",
		"Danny",
	}
	beeper.Add(len(targets))
	for _, target := range targets {
		go attackTarget(&beeper, target)
	}
	beeper.Wait()
}

func attackTarget(beeper *sync.WaitGroup, target string) {
	defer beeper.Done()
	fmt.Println("Attacked target", target)
}

func moreProblems() {
	// indefinitely waiting
	//var beeper sync.WaitGroup
	//beeper.Add(1)
	//beeper.Wait()

	// negative wg counter
	//var beeper sync.WaitGroup
	//beeper.Add(1)
	//beeper.Done()
	//beeper.Done()
	//beeper.Wait()

	// add on another goroutine
	var beeper sync.WaitGroup
	beeper.Add(1)
	go func() {
		beeper.Add(1)
		beeper.Done()
		beeper.Done()
	}()
	beeper.Wait()
}