package main

import (
	"fmt"
	"math/rand"
	"time"
)

func main() {
	channel := make(chan string)
	numTasks := 3
	go task(channel, numTasks)

	for i := 0; i < numTasks; i++ {
		fmt.Println(<-channel)
	}

	// what if we don't get to know exactly how many tasks?
	//for message := range channel {
	//	fmt.Println(message)
	//}

	// alternatively, we can rewrite the for loop in a more manual way
	// checking if the channel is still open
	//for {
	//	message, open := <-channel
	//	if !open {
	//	break
	//}
	//	fmt.Println(message)
	//}
}

func task(channel chan string, numTasks int) {
	// defer close(channel)
	for i := 0; i < numTasks; i++ {
		channel <- fmt.Sprint("Task:", i)
	}
	//close(channel)
}

func throwNinjaStars(channel chan string) {
	rand.Seed(time.Now().UnixNano())
	channel <- fmt.Sprint("You scored:", rand.Intn(10))
}