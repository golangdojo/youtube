package main

import (
	"fmt"
	"time"
)

func main() {
	intrude()

	var attacked chan string
	// attacked <- true
	attacked = make(chan string)

	//var attackedSent <-chan string
	//attackedSent = attacked
	//<-attackedSent

	go attack("Tommy", attacked)

	// unidirectional channel - send and receive, not conversion the other way

	//var attackedReceived <-chan bool
	//attackedReceived = attacked
	//<-attackedReceived
	//<-attacked

	retrieve(attacked)

	// we can make the channel send and receive struct{}{}
	// since we aren't using the result, or if we are hella short on memory
	attacked <- "Attacked!"

	// draw channels between processes

	//attacked <- "Attacked!"
	// deadlock, sub for buffered channels
}

func attack(target string, attacked chan<- string) {
	fmt.Println("Throwing ninja stars at", target)
	time.Sleep(time.Second)
	attacked <- "Attacked!"

	//<-attacked, can't receive on sending channel
}

func intrude() {
	fmt.Println("Mission started")
	fmt.Println(time.Now())
}

func retrieve(attacked <-chan string) {
	<-attacked
	//fmt.Println(<-attacked)
	fmt.Println("Mission completed")
}