package main

import (
	"fmt"
	"sync"
	"time"
)

var (
	lock   sync.Mutex
	rwlock sync.RWMutex
	count  int
)

func main() {

	// https://stackoverflow.com/questions/34524/what-is-a-mutex

	basics()
	readAndWrite()
	//writeAndRead()
	//multipleReads()
	//sameReads()             // *may* deadlock
	//recursiveReads(3) // *may* deadlock
	// https://pkg.go.dev/sync#RWMutex No guarantee for recursive
	//time.Sleep(5 * time.Second)
}

func basics() {
	iterations := 100
	//iterations := 10000
	for i := 0; i < iterations; i++ {
		//go increment()
		go safelyIncrement()
	}
	time.Sleep(1 * time.Second)
	fmt.Println("Resulted count is:", count)
}

func increment() {
	count++
}

func safelyIncrement() {
	lock.Lock()
	defer lock.Unlock()
	increment()
}

func readAndWrite() {
	go read()
	go write()

	time.Sleep(5 * time.Second)
	fmt.Println("Done")
}

func writeAndRead() {
	go write()
	go read()

	time.Sleep(5 * time.Second)
	fmt.Println("Done")
}

func multipleReads() {
	go read()
	go read()
	go read()
	//go write()

	time.Sleep(5 * time.Second)
	fmt.Println("Done")
}


func sameReads() {
	rwlock.RLock()
	rwlock.RLock()
	rwlock.RUnlock()
	rwlock.RUnlock()
}

func recursiveReads(count int) {
	if count == 0 {
		return
	}

	rwlock.RLock()
	defer rwlock.RUnlock()

	fmt.Println("Read locking")

	recursiveReads(count - 1)

	time.Sleep(1 * time.Second)
	fmt.Println("Reading unlocking")
}

func read() {
	rwlock.RLock()
	defer rwlock.RUnlock()

	fmt.Println("Read locking")
	time.Sleep(1 * time.Second)
	fmt.Println("Reading unlocking")
}

func write() {
	rwlock.Lock()
	defer rwlock.Unlock()

	fmt.Println("Write locking")
	time.Sleep(1 * time.Second)
	fmt.Println("Write unlocking")
}
