package main

import (
	"fmt"
	"sync"
)

func main() {
	// Golang Concurrency - Resource Pool

	var numMemPieces int
	memPool := &sync.Pool{
		New: func() interface{} {
			numMemPieces++
			mem := make([]byte, 1024)
			return &mem
		},
	}

	const numWorkers = 1024 * 1024
	// imagine spawning this many regular threads in java
	// your machine is gonna explode
	var wg sync.WaitGroup
	wg.Add(numWorkers)
	for i := 0; i < numWorkers; i++ {
		go func() {
			defer wg.Done()

			mem := memPool.Get().(*[]byte)
			//fmt.Sprintln("Taking some time on the resource.")
			defer memPool.Put(mem)
		}()
	}
	wg.Wait()
	fmt.Printf("%d numMemPieces were created", numMemPieces)
}
