package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

var (
	ready bool
)

func main() {
	// title: Golang Concurrency - Signal & Broadcast

	gettingReadyForMission()
	gettingReadyForMissionWithCond()
	broadcastStartOfMission()
}

func broadcastStartOfMission() {
	m := sync.Mutex{}
	broadcaster := sync.NewCond(&m)
	var wg sync.WaitGroup
	wg.Add(3)
	standByForMission(func() {
		fmt.Println("Ninja 1 starting mission.")
		wg.Done()
	}, broadcaster)
	standByForMission(func() {
		fmt.Println("Ninja 2 starting mission.")
		wg.Done()
	}, broadcaster)
	standByForMission(func() {
		fmt.Println("Ninja 3 starting mission.")
		wg.Done()
	}, broadcaster)
	broadcaster.Broadcast()
	wg.Wait()
	fmt.Println("All Ninjas have started their missions")
}

func standByForMission(fn func(), broadcaster *sync.Cond) {
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		wg.Done()
		broadcaster.L.Lock()
		defer broadcaster.L.Unlock()
		broadcaster.Wait()
		fn()
	}()
	wg.Wait()
}

func gettingReadyForMission() {
	defer elapsed("Getting ready")()

	go gettingReady()
	workIntervals := 0
	for !isReady() {
		workIntervals++
	}
	fmt.Printf("We are now ready! After %d work intervals.\n", workIntervals)
}

func gettingReadyForMissionWithCond() {
	defer elapsed("Getting ready")()

	c := sync.NewCond(&sync.Mutex{})

	go gettingReadyWithCond(c)
	workIntervals := 0
	c.L.Lock()
	// slightly different implementation of a synchronization primitive like a mutex
	// here you are release the old lock after acquiring a new one
	// and you lock it back after getting notified eventually and attempts to
	// exit the wait method
	for !isReady() {
		workIntervals++
		c.Wait()
	}
	c.L.Unlock()
	// think of it without the locking and unlocking, you only call the wait function
	// and you are waiting a signal to continue
	fmt.Printf("We are now ready! After %d work intervals.\n", workIntervals)
}

func gettingReady() {
	sleep()
	ready = true
}

func gettingReadyWithCond(c *sync.Cond) {
	sleep()
	ready = true
	c.Signal()
}

func sleep() {
	rand.Seed(time.Now().UnixNano())
	someTime := time.Duration(1 + rand.Intn(5)) * time.Second
	time.Sleep(someTime)
}

func isReady() bool {
	return ready
}

func elapsed(what string) func() {
	start := time.Now()
	return func() {
		fmt.Printf("%s took %v\n", what, time.Since(start))
	}
}
