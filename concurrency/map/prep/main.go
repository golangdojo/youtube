package main

import (
	"fmt"
	"sync"
)

func main() {
	// Golang Concurrency - Map Performance & Safety
	// using map as an example
	// https://pkg.go.dev/sync@master#Map
	m := make(map[int]int)
	for i := 0; i < 10; i++ {
		go func() {
			m[0] = i
			fmt.Println(m[0])
		}()
	}

	syncMap := sync.Map{}
	regularMap := make(map[int]interface{})

	// put
	syncMap.Store(0, 0)
	syncMap.Store(1, 1)
	syncMap.Store(2, 2)
	regularMap[0] = 0
	regularMap[1] = 1
	regularMap[2] = 2

	// get
	syncValue, ok := syncMap.Load(0)
	regularValue := regularMap[0]
	fmt.Println(syncValue, ok, regularValue)

	// delete
	syncMap.Delete(1)
	regularMap[1] = nil

	// get and delete
	syncValue, loaded := syncMap.LoadAndDelete(2)
	regularValue = regularMap[2]
	delete(regularMap, 2)
	fmt.Println(syncValue, loaded, regularValue)
	newSyncValue, ok := syncMap.Load(2)
	newRegularValue := regularMap[2]
	fmt.Println(newSyncValue, ok, newRegularValue)

	// get and put
	syncValue, loaded = syncMap.LoadOrStore(1, 1)
	regularValue = regularMap[1]
	if regularValue == nil {
		regularMap[1] = 1
		regularValue = regularMap[1]
	}
	fmt.Println(syncValue, loaded, regularValue)

	// range
	syncMap.Range(func(key, value interface{}) bool {
		fmt.Print(key, value, " | ")
		return true
	})
	fmt.Println()
	for key, value := range regularMap {
		fmt.Print(key, value, " | ")
	}
	fmt.Println()
}
