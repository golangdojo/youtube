package main

import (
	"sync"
	"testing"
)

const iterations = 1000
//const iterations = 100000

func BenchmarkRegularMap(t *testing.B) {
	var mutex sync.Mutex
	regularMap := make(map[int]int, iterations)
	var wg sync.WaitGroup
	wg.Add(iterations - 1)
	regularMap[0] = 0

	for i := 1; i < iterations; i++ {
		go func() {
			defer wg.Done()
			//j := 1
			for j := 1; j < iterations; j++ {
				go func() {
					mutex.Lock()
					defer mutex.Unlock()
					regularMap[i] = j
				}()
			}
		}()
	}

	wg.Wait()
}

func BenchmarkSyncMap(t *testing.B) {
	var syncMap sync.Map
	var wg sync.WaitGroup
	wg.Add(iterations - 1)
	syncMap.Store(0, 0)

	for i := 1; i < iterations; i++ {
		go func() {
			defer wg.Done()
			//j := 1
			for j := 1; j < iterations; j++ {
				go func() {
					syncMap.Store(i, j)
					// concurrent loops with keys that are stable over time,
					// and either few steady-state stores,
					// or stores localized to one goroutine per key.
					// For use cases that do not share these attributes,
					// it will likely have comparable or worse performance and
					// worse type safety than an ordinary map paired with a read-write mutex
				}()
			}
		}()
	}

	wg.Wait()
}
