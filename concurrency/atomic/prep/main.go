package main

import (
	"fmt"
	"math/rand"
	"sync"
	"sync/atomic"
	"time"
)

func main() {
	//basics()

	// use case for atomic.Value https://texlution.com/post/golang-lock-free-values-with-atomic-value/
	//randomNinjaWeaponVendor()
	ninjaOp()
}

func basics() {
	var sum int64
	atomic.AddInt64(&sum, 1)
	var sum2 int32
	atomic.AddInt32(&sum2, 1)

	// instead of mutex
	var mu1 sync.Mutex
	mu1.Lock()
	sum = sum + 1
	mu1.Unlock()
	var mu2 sync.Mutex
	mu2.Lock()
	sum2 = sum2 + 1
	mu2.Unlock()


	fmt.Println(atomic.LoadInt64(&sum))
	// What's the point?
	// It's specially implemented for system architectures that
	// don't have loads or stores implemented atomically

}

func ninjaOp() {
	var v atomic.Value
	v.Load()
	v.Store(ninja{})

	v.Store(ninja{"Wallace"})
	var wg sync.WaitGroup
	wg.Add(1)
	go func() {
		wallace := v.Load().(ninja)
		wallace.name = "Not Wallace"
		v.Store(wallace)
		wallace.name = "Wallace Again"
		wg.Done()
	}()
	wg.Wait()
	fmt.Println(v.Load().(ninja).name)
}

type ninja struct {
	name string
}

func randomNinjaWeaponVendor() {
	var v atomic.Value
	var wg sync.WaitGroup
	wg.Add(1)

	go gainExperiencePointsDuringMissions(&v, &wg)
	wg.Wait()

	wg.Add(10)
	for numNinja := 0; numNinja < 10; numNinja++ {
		go func() {
			weapons := v.Load().([]string)
			fmt.Println("You got", weapons, "this time!")
		}()
	}
	wg.Wait()
}

func gainExperiencePointsDuringMissions(v *atomic.Value, wg *sync.WaitGroup) {
	v.Store(getNinjaWeapons())
	wg.Done()
	for { // can use cond for signaling to exit more gracefully here
		// heck out my last video to learn more
		v.Store(getNinjaWeapons())
	}
}

func getNinjaWeapons() []string {
	rand.Seed(time.Now().UnixNano())
	weapons := []string {
		"Throwing Star",
		"Samurai",
		"Battle Axe",
		"Longbow",
		"Hammer",
		"Smoke bomb",
	}
	return []string{
		weapons[rand.Intn(len(weapons))],
		weapons[rand.Intn(len(weapons))],
	}
}
