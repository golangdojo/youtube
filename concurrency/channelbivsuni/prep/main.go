package main

import "fmt"

func main() {
	results := make(chan string)
	acceptances := make(chan string)
	go ninjaOne(results, acceptances)
	go ninjaTwo(results, acceptances)

	results <- "Ninja one offer"
	results <- "Ninja two offer"
	close(results)
	for acceptance := range acceptances {
		fmt.Println(acceptance)
	}
}

func ninjaOne(results <-chan string, acceptances chan<- string) {
	for result := range results {
		fmt.Println(result)
	}
	acceptances <- "I accept!"
}

func ninjaTwo(results <-chan string, acceptances chan<- string) {
	for result := range results {
		fmt.Println(result)
	}
	acceptances <- "I accept!"
}
