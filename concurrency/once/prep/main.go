package main

import (
	"fmt"
	"math/rand"
	"sync"
	"time"
)

func main() {
	// title: Golang Concurrency - Do It "Once" Only [sync.Once]

	treasureMission()
}

var missionCompleted bool

func treasureMission() {
	var once sync.Once

	for i := 0; i < 100; i++ {
		go func() {
			if foundTreasure() {
				once.Do(markMissionCompleted)
			}
		}()
	}

	if missionCompleted {
		fmt.Println("Mission is now completed.")
	} else {
		fmt.Println("Mission was a failure.")
	}
}

func foundTreasure() bool {
	rand.Seed(time.Now().UnixNano())
	return 0 == rand.Intn(10)
}

func markMissionCompleted() {
	missionCompleted = true
}
