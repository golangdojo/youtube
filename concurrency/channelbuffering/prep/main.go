package main

import (
	"fmt"
	"strconv"
)

func main() {
	// draw a graph, it's like a fixed sized slice
	// that said, default channels are 0 sized channels
	// there isn't any buffer to temporarily store additional messages

	bufferedSize := 2

	channel := make(chan string, bufferedSize)

	for i := 0; i < bufferedSize; i++ {
		channel <- strconv.Itoa(i)
		fmt.Println("Channel length:", len(channel))
		fmt.Println("Channel capacity:", cap(channel))
	}

	for i := 0; i < bufferedSize; i++ {
		fmt.Println(<-channel)
	}

	// deadlock again, out of buffer
	// 	fmt.Println(<-channel)

	// integrate with multiple goroutines
}

