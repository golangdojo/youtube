package main

import (
	"fmt"
	"time"
)

func main() {
	intrude()

	start := time.Now()
	defer func() {
		fmt.Println(time.Since(start))
	}()

	// evil ninja to ... take care of
	targets := []string{"Tommy", "Johnny", "Bobby", "Andy"}
	for _, target := range targets {
		go attack(target)
	}

	retrieve()
}

func attack(target string) {
	fmt.Println("Throwing ninja stars at", target)
	time.Sleep(time.Second)
}

func intrude() {
	fmt.Println("Mission started")
	fmt.Println(time.Now())
}

func retrieve() {
	// wasted  time is server costs/money left on the table to buy ninja weapons
	time.Sleep(time.Second)

	fmt.Println("Mission completed")

	// nanoseconds since process start
	fmt.Println(time.Now())
}