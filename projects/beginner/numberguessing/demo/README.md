# Guessing Game

## Description
A game where a player can guess what the secret number is. 

The computer will let the player know if the guess is greater or smaller the secret number.

[//]: # (## Requirements)

[//]: # (- Establish a project)

[//]: # (- Create secret number)

[//]: # (- Get player input)

[//]: # (- Make comparison &#40;input vs secret&#41;)

[//]: # (- Loop the input until correct)