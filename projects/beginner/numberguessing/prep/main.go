package main

import (
	"fmt"
	"math/rand"
	"strconv"
	"time"
)

func main() {
	compare := -1
	random := getRandomNumber()
	for compare != 0 {
		//guess := getUserInputValidated()
		//compare = makeComparison(guess, random)
		guess, ok := getUserInputValidated()
		if ok {
			compare = makeComparison(guess, random)
		}
	}
}

func getUserInput() int {
	fmt.Printf("Please input your guess: ")
	var guess int
	_, err := fmt.Scan(&guess)
	if err != nil {
		fmt.Println("Failed to parse your guess")
	} else {
		fmt.Println("You guessed:", guess)
	}
	return guess
}

func getUserInputValidated() (int, bool) {
	fmt.Printf("Please enter your guess: ")
	var guessString string
	_, err := fmt.Scan(&guessString)
	guessInt, err := strconv.Atoi(guessString)
	if err != nil {
		fmt.Println("Failed to parse your guess")
		return guessInt, false
	}
	fmt.Println("You guessed:", guessInt)

	return guessInt, true
}

func getRandomNumber() int {
	rand.Seed(time.Now().Unix())
	return rand.Int() % 10
}

func makeComparison(guess, actual int) int {
	if guess > actual {
		fmt.Println("Your guess is too big")
		return 1
	} else if guess < actual {
		fmt.Println("Your guess is too small")
		return -1
	} else {
		fmt.Println("You got it!")
		return 0
	}
}
