package main

import (
	"fmt"
	"log"
)

type ai struct {
	memory      []string
	memoryLevel int
}

func main() {

	var npc = ai{
		memory:      make([]string, 5),
		memoryLevel: 5,
	}

}

func getPlayerInput() string {
	var input string
	_, err := fmt.Scan(&input)
	if err != nil {
		log.Fatal(err)
	}
	return input
}

func learn() {

}

func reactiveAi() string {

}
