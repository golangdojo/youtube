package main

import (
	"bufio"
	"encoding/csv"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
)

type Question struct {
	Q string
	A string
}

type Quiz struct {
	questions []Question
}

func main() {
	quiz := parseQuizFile()
	var correctCount int
	for _, question := range quiz.questions {
		fmt.Printf("%s ", question.Q)
		input := getPlayerInput()
		if isMatching(question.A, input) {
			correctCount++
		}
	}
	score := calculateScore(correctCount, len(quiz.questions))
	fmt.Printf("You scored: %d%%!\n", score)
}

func parseQuizFile() *Quiz {
	file, err := os.Open("quiz.csv")
	if err != nil {
		log.Fatal("Failed to parse quiz file")
	}
	questions := make([]Question, 0)
	reader := csv.NewReader(bufio.NewReader(file))
	for {
		line, err := reader.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			log.Fatal(err)
		}
		questions = append(questions, Question{
			Q: line[0],
			A: line[1],
		})
	}
	return &Quiz{questions: questions}
}

func getPlayerInput() string {
	var input string
	_, err := fmt.Scan(&input)
	if err != nil {
		log.Fatal(err)
	}
	return input
}

func isMatching(answer, input string) bool {
	answer = strings.ToLower(answer)
	input = strings.ToLower(input)
	return strings.Contains(answer, input)
}

func calculateScore(correctCount, totalCount int) int {
	return correctCount * 100 / totalCount
}
