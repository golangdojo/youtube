package main

import (
	"math/rand"
	"net/url"
	"time"
)

const charset = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"

var seededRand *rand.Rand = rand.New(
	rand.NewSource(time.Now().UnixNano()))

type Codec struct {
	base string
	urls map[string]string
}

func Constructor() Codec {
	return Codec{
		base:"http://tinyurl.com",
		urls: map[string]string{},
	}
}

// Encodes a URL to a shortened URL.
func (this *Codec) encode(longUrl string) string {
	b := make([]byte, 8)
	for i := range b {
		b[i] = charset[seededRand.Intn(len(charset))]
	}
	hash := "/" + string(b)
	this.urls[hash] = longUrl
	return this.base + hash
}

// Decodes a shortened URL to its original URL.
func (this *Codec) decode(shortUrl string) string {
	u, err := url.Parse(shortUrl)
	if err != nil {
		panic(err)
	}
	return  this.urls[u.Path]
}

func main() {

}
