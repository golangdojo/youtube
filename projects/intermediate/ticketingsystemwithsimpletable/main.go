package main

import "flag"

type TicketStore struct {
	Tickets []Ticket
}

type Ticket struct {
	status Status
}

type Status int

const (
	CREATED Status = iota
	IN_PROGRESS
	COMPLETED
)

const (
	TicketStoreJson = "ticket_store.json"
)

func init() {
	
}

func main() {
	create := flag.Bool("create", false, "create a new ticket")
	work := flag.Int("work", -1, "work on a created ticket")
	complete := flag.Int("complete", -1, "complete an in-progress ticket")
	delete := flag.Int("delete", -1, "delete an existing ticket")
	list := flag.Bool("list", false, "list all tickets")

	switch {
		*create:

	}
}
