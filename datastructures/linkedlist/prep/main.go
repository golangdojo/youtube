package main

import (
	"fmt"
	"strings"
)

type node struct {
	value int
	next  *node
}

type linkedList struct {
	head   *node
	length int
}

func (l *linkedList) append(value int) {
	newNode := new(node)
	newNode.value = value

	if l.head == nil {
		l.head = newNode
	} else {
		iterator := l.head
		for ; iterator.next != nil; iterator = iterator.next {
		}
		iterator.next = newNode
	}
}

func (l *linkedList) remove(value int) {
	var previous *node

	for current := l.head; current != nil;
	current = current.next {
		if current.value == value {
			if l.head == current {
				l.head = current.next
			} else {
				previous.next = current.next
				// can instantiate previous, but it might also be easier to debug
			}
			return
		}
		previous = current
	}
}

func (l *linkedList) removeAll(value int) {
	var previous *node

	for current := l.head; current != nil;
	current = current.next {
		if current.value == value {
			if l.head == current {
				l.head = current.next
			} else {
				previous.next = current.next
				// can instantiate previous, but it might also be easier to debug
			}
		} else {
			previous = current
		}
	}
}

func (l linkedList) len() int {
	len := 0
	for iterator := l.head; iterator != nil; iterator =  iterator.next {
		len++
	}
	return len
}
// or we can increment or decrement, which is usually favorable

func (l linkedList) String() string {
	builder := strings.Builder{}
	for iterator := l.head; iterator != nil; iterator = iterator.next {
		builder.WriteString(fmt.Sprintf("%d ", iterator.value))
	}
	return builder.String()
}

func main() {
	l := linkedList{}
	l.append(1)
	l.append(2)
	l.append(3)
	l.append(4)
	l.append(5)
	fmt.Println(l)
	l.remove(1)
	l.remove(3)
	l.remove(5)
	fmt.Println(l)
	l.remove(7)
	fmt.Println(l)
	l.append(5)
	l.append(5)
	fmt.Println(l)
	l.removeAll(5)
	fmt.Println(l)
}