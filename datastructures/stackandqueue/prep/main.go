package main

import (
	"fmt"
	"strings"
)

type stack struct {
	elements []interface{}
}

func (s *stack) push(value interface{}) {
	s.elements = append(s.elements, value)
}

func (s *stack) pop() interface{} {
	popped := s.elements[len(s.elements) - 1]
	s.elements = s.elements[:len(s.elements) - 1]
	return popped
}

func (s stack) String() string {
	sb := strings.Builder{}
	for _, element := range s.elements {
		sb.WriteString(fmt.Sprintf("%s, ", element))
	}
	return sb.String()
}

type queue struct {
	elements []interface{}
}

func (q *queue) enqueue(value interface{}) {
	q.elements = append(q.elements, value)
}

func (q *queue) dequeue() interface{} {
	popped := q.elements[0]
	q.elements = q.elements[1:]
	return popped
}

func (q queue) String() string {
	sb := strings.Builder{}
	for _, element := range q.elements {
		sb.WriteString(fmt.Sprintf("%s, ", element))
	}
	return sb.String()
}

func main() {
	s := stack{}
	s.push("Hello")
	s.push(3)
	s.push("World")
	fmt.Println(s)
	fmt.Println(s.pop())
	fmt.Println(s)

	fmt.Println()

	q := queue{}
	q.enqueue("Hello")
	q.enqueue("World")
	fmt.Println(q)
	fmt.Println(q.dequeue())
	fmt.Println(q)

}
