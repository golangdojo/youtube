# Go Environment Variables

## What are environment variables?
- String variables
- Informs programs about system setup

## Go specific environment variables
- GOROOT - where go runtime live
- GOPATH -  where local go projects live
- PATH - creating shortcuts for executables you may use frequently

## Folders during Go installation
- /go (/src, /bin, etc.)
- /goworkspace (/src, /bin, etc.)

 ## How to set them?

- `#` indicates it's a comment
- `$` indicates it's an existing variable
- ```
  # ~/.bashrc on Linux
  export GOROOT=/usr/local/go
  export GOPATH=$HOME/goworkspace
  export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
  ```