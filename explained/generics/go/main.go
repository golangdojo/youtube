package main

import (
	"fmt"
)

type Type interface {
	type int, string
}

func print[T interface{}](output ...T) {
	fmt.Println(output)
}

func print(output ...interface{}) {
	fmt.Println(output)
}

func main() {
	print(1, 2, 3)
	print(1, 2.0, "3")
}

func isGreaterThan(x int, y int) bool {
	return x > y
}