import java.util.ArrayList;

class Main {
    public static void main(String[] args) {
    }

    private static boolean isGreaterThan(int x, int y) {
        return x > y;
    }

    private static boolean isGreaterThan(double x, double y) {
        return x > y;
    }

    private static <T extends Comparable> boolean isGreaterThan(T x, T y) {
        return x.compareTo(y) > 0;
    }
}




