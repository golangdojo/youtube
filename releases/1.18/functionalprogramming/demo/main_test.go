package main

import "testing"

func BenchmarkFilterWithFP(b *testing.B) {
	intSlice := []int{1, 4, -3, 5, -9, 3, -1}
	for i := 0; i < b.N; i++ {
		intStream := Stream[int]{intSlice}
		intStream.Filter(IsPositive[int]).Filter(IsEven[int])
	}
}

func BenchmarkFilterWithLoops(b *testing.B) {
	intSlice := []int{1, 4, -3, 5, -9, 3, -1}
	for i := 0; i < b.N; i++ {
		var positiveIntSlice []int
		for _, v := range intSlice {
			if v > 0 {
				positiveIntSlice = append(positiveIntSlice, v)
			}
		}
		var result []int
		for _, v := range positiveIntSlice {
			if v%2 == 0 {
				result = append(result, v)
			}
		}
	}
}

func BenchmarkFilterWithOneLoop(b *testing.B) {
	intSlice := []int{1, 4, -3, 5, -9, 3, -1}
	for i := 0; i < b.N; i++ {
		var result []int
		for _, v := range intSlice {
			if v > 0 && v%2 == 0 {
				result = append(result, v)
			}
		}
	}
}
