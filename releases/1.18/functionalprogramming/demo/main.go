package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

func main() {
	// .Filter() .map() .reduce()
	// positiveStreamVariable = someStreamVariable.Filter(v -> v > 0)
	intSlice := []int{1, 4, -3, 5, -9, 3, -1}
	positiveIntSlice := Filter[int](intSlice, IsPositive[int])
	fmt.Println(positiveIntSlice)

	intStream := Stream[int]{intSlice}
	intStream.Filter(IsPositive[int]).Filter(IsEven[int])
	fmt.Println(intStream)
}

type Stream[T Number] struct {
	s []T
}

func (s *Stream[T]) Filter(f func(T) bool) *Stream[T] {
	s.s = Filter(s.s, f)
	return s
}

type Number interface {
	constraints.Integer | constraints.Float
}

func Filter[T Number](s []T, f func(T) bool) []T {
	var r []T
	for _, v := range s {
		if f(v) {
			r = append(r, v)
		}
	}
	return r
}

func IsPositive[T Number](v T) bool {
	return v > 0
}

func IsEven[T constraints.Integer](a T) bool {
	return (a % 2) == 0
}
