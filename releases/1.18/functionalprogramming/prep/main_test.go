package main

import "testing"

var intSlice = []int{1, 4, -3, 5, -9, 3, -1}

func BenchmarkFilterWithFP(b *testing.B) {
	for i := 0; i < b.N; i++ {
		filterWithFP(intSlice)
	}
}

func BenchmarkFilterWithLoops(b *testing.B) {
	for i := 0; i < b.N; i++ {
		filterWithLoops(intSlice)
	}
}

func BenchmarkFilterWithLoop(b *testing.B) {
	for i := 0; i < b.N; i++ {
		filterWithLoop(intSlice)
	}
}
