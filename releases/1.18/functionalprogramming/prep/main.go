package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

func main() {
	intSlice := []int{1, 4, -3, 5, -9, 3, -1}
	positiveIntSlice := Filter[int](intSlice, IsPositive[int])
	fmt.Println(positiveIntSlice)
	floats := []float64{1.0, 4.1, -3.8, 5.3, -9.0, 3.5, -1.0}
	positiveFloatSlice := Filter[float64](floats, IsPositive[float64])
	fmt.Println(positiveFloatSlice)

	intStream := Stream[int]{intSlice}
	positiveIntStream := intStream.filter(IsPositive[int])
	fmt.Println(positiveIntStream)
	positiveEvenIntStream := intStream.filter(IsPositive[int]).filter(IsEven[int])
	fmt.Println(positiveEvenIntStream)
}

func filterWithFP[T constraints.Integer](s []T) []T {
	intStream := Stream[T]{s}
	return intStream.filter(IsPositive[T]).filter(IsEven[T]).s
}

func filterWithLoops(s []int) []int {
	var result []int
	for _, v := range s {
		if v < 0 {
			result = append(result, v)
		}
	}
	for _, v := range s {
		if v%2 == 0 {
			result = append(result, v)
		}
	}
	return result
}

func filterWithLoop(s []int) []int {
	var result []int
	for _, v := range s {
		if v < 0 && v%2 == 0 {
			result = append(result, v)
		}
	}
	return result
}

type RealNumber interface {
	constraints.Integer | constraints.Float
}

func IsPositive[T RealNumber](a T) bool {
	return a > 0
}

func IsEven[T constraints.Integer](a T) bool {
	return (a % 2) == 0
}

type Stream[T RealNumber] struct {
	s []T
}

func (s *Stream[T]) filter(f func(T) bool) *Stream[T] {
	s.s = Filter(s.s, f)
	return s
}

func Filter[T any](s []T, f func(T) bool) []T {
	var r []T
	for _, v := range s {
		if f(v) {
			r = append(r, v)
		}
	}
	return r
}
