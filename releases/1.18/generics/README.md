# Generics...FINALLY!

## Type Parameter
```
func min[T any] (a T, a T) T {
    // …
}

func main() {
    fmt.Println(min[int](1, 2)) // Prints out "1"
}


```

## Type Inference
```
func main() {
    fmt.Println(min(1, 2)) // Removed `[int]`
}

```

## Type Set
```
type minTypes interface {
    int | float64
}

func main() {
    fmt.Println(min(1, 2))         // Compiles
    fmt.Println(min(0.1, 0.2))     // Compiles
    fmt.Println(min("0.1", "0.2")) // Doesn't compile
}

```