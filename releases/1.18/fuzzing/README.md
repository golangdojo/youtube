# Fuzzing

- A feature in the `testing` package
- Fuzzing is a type of automated testing which continuously manipulates inputs to a program to find bugs
