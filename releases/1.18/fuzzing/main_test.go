package main

import "testing"

// We will cover testing in Go extensively soon
func FuzzAdd(f *testing.F) {
	f.Add(1, 2, 1+2)
	f.Add(2, 5, 2+5)
	f.Add(3, 2, 3+2)
	//f.Add(3, 2, 3+1) // This one will fail
	f.Fuzz(func(t *testing.T, a, b, sum int) {
		if a+b != sum {
			t.Errorf("%d + %d != %d\n", a, b, sum)
		}
	})
}

func TestAdd(t *testing.T) {
	if add(1, 2) != 3 {
		t.Error("Something wrong with add()")
	}
}
