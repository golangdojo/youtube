[//]: # (# Generics)

[//]: # ()
[//]: # (## What is Generics?)

[//]: # (Generics allows types to be parameters just like variables to functions)

[//]: # ()
[//]: # (## Performance?)

[//]: # (One of the biggest hesitations &#40;along with readability&#41;)

[//]: # ()
[//]: # (### Strategy)

[//]: # (- Alternatives)

[//]: # (  - Type switch & type assertion)

[//]: # (    ```)

[//]: # (    i, ok := v.&#40;int&#41;)

[//]: # (    f, ok := v.&#40;float64&#41;)

[//]: # (    s, ok := v.&#40;string&#41;)

[//]: # (    ```)

[//]: # (  - Reflection & type assertion)

[//]: # (    ```)

[//]: # (    ok := reflect.TypeOf&#40;v&#41;.Kind&#40;&#41; == reflect.Int)

[//]: # (    ok := reflect.TypeOf&#40;v&#41;.Kind&#40;&#41; == reflect.Float64)

[//]: # (    ok := reflect.TypeOf&#40;v&#41;.Kind&#40;&#41; == reflect.String)

[//]: # (    ```)

[//]: # (  - Explicit)

[//]: # (    ```)

[//]: # (    func f&#40;v int&#41; {})

[//]: # (    func f&#40;v float64&#41; {})

[//]: # (    func f&#40;v string&#41; {})

[//]: # (    ```)

[//]: # (  - Generics)

[//]: # (    ```)

[//]: # (    func f[T int|float64|string]&#40;v T&#41; {})

[//]: # (    ```)

[//]: # (    ```)

[//]: # (    type typeSet interface {)

[//]: # (        int|float64|string)

[//]: # (    })

[//]: # (    func f[T typeSet]&#40;v T&#41; {})

[//]: # (    ```)

[//]: # ()
[//]: # (- Function Implementations)

[//]: # (  - `add&#40;a, b&#41;`)

### Conclusion
- Performance
       
  ```
  254.4 ns/op  BenchmarkAddExplicit
  254.8 ns/op  BenchmarkAddGenerics
  254.9 ns/op  BenchmarkAddGenericsWithTypeSet
  259.9 ns/op  BenchmarkAddTypeSwitch
  511.4 ns/op  BenchmarkAddReflection                      
  ```           


