package main

import (
	"math/rand"
	"strconv"
	"testing"
)

func BenchmarkAddTypeSwitch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		addTypeSwitch(rand.Int(), rand.Int())
		addTypeSwitch(rand.Float64(), rand.Float64())
		addTypeSwitch(strconv.Itoa(rand.Int()), strconv.Itoa(rand.Int()))
	}
}

func BenchmarkAddReflection(b *testing.B) {
	for i := 0; i < b.N; i++ {
		addReflection(rand.Int(), rand.Int())
		addReflection(rand.Float64(), rand.Float64())
		addReflection(strconv.Itoa(rand.Int()), strconv.Itoa(rand.Int()))
	}
}

func BenchmarkAddExplicit(b *testing.B) {
	for i := 0; i < b.N; i++ {
		addInt(rand.Int(), rand.Int())
		addFloat64(rand.Float64(), rand.Float64())
		addString(strconv.Itoa(rand.Int()), strconv.Itoa(rand.Int()))
	}
}

func BenchmarkAddGenerics(b *testing.B) {
	for i := 0; i < b.N; i++ {
		addGenerics(rand.Int(), rand.Int())
		addGenerics(rand.Float64(), rand.Float64())
		addGenerics(strconv.Itoa(rand.Int()), strconv.Itoa(rand.Int()))
	}
}

func BenchmarkAddGenericsWithTypeSet(b *testing.B) {
	for i := 0; i < b.N; i++ {
		addGenericsWithTypeSet(rand.Int(), rand.Int())
		addGenericsWithTypeSet(rand.Float64(), rand.Float64())
		addGenericsWithTypeSet(strconv.Itoa(rand.Int()), strconv.Itoa(rand.Int()))
	}
}
