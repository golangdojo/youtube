# Golang Dojo
Golang Dojo is all about mastering the Go programming language and becoming Golang Ninjas together :)

Get Your Golang Cheat Sheet! - https://golangdojo.com/cheatsheet

Notes & resources - https://golangdojo.com/resources

## About
This repository contains all the necessary files used in Golang Dojo's YouTube channel, to make your life a bit simpler when learning Golang!

## Table of Content
### basics
What Golang developers must learn. It's mostly based on "The Go Programming Language". Great book, you should check it out :)

### underthehood
How Golang works under the hood. You need to learn these to become a Golang Ninja! :)

### datastructures
How else will you find a job if you don't know how to revert a binary tree!?

### concurrency
One of the biggest selling point of Go. Don't miss out!

### webapp
Building web apps is one of the most common usages of Go, so...

## Getting Started

Video guide - https://youtu.be/VTJ8qnBevcs

Latest version of Go – https://golang.org/dl

Best free Go code editor – https://code.visualstudio.com

Best Go IDE – https://jetbrains.com/go
