package main

import "fmt"

type ninjaStar struct {
	owner string
}

type ninjaSword struct {
	owner string
}

func (ninjaStar) attack() {
	fmt.Println("Throwing ninja star")
}

func (ninjaStar) pickup() {
	fmt.Println("Picking up ninja star")
}

func (ninjaSword) attack() {
	fmt.Println("Swinging ninja sword")
}

type ninjaWeapon interface {
	attack()
}

func main() {
	weapons := []ninjaWeapon{ninjaStar{"Wallace"}}
	//weapons = []ninjaWeapon{ninjaStar{"Wallace"}, ninjaSword{"Wallace"}}
	for _, weapon := range weapons {
		weapon.(ninjaStar).pickup()
	}

	weapons = []ninjaWeapon{ninjaStar{"Wallace"}, ninjaSword{"Wallace"}}
	for _, weapon := range weapons {
		ns, ok := weapon.(ninjaStar)
		if ok {
			ns.pickup()
		} else {
			ns.pickup()
			fmt.Println("-" + ns.owner + "-")
		}
	}
}

func pickup(weapons []interface{}) {
	for _, weapon := range weapons {
		weapon.(ninjaStar).pickup()
	}
}
