package main

import "fmt"

type ninja struct {
	name string
	level int
	weapon string
}


func (n ninja) String() string {
	return fmt.Sprintf("I'm level %d ninja %s, and I use a %s",
		n.level, n.name, n.weapon)
}


func main() {
	// Title: Golang toString() - Stringer interface in Go

	// Today's video is going to be a short one

	wallace := ninja{"Wallace", 1, "Ninja Star"}
	fmt.Println(wallace) // find .String() called

}
