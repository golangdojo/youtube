package main

import "fmt"

func main() {

	/*
	title: Golang Loops - for loop, while loop, foreach, label

	intro:
	loops are some of the most basic building blocks of any programming
	project. no exception in go, well especially in the golang since we don't
	have much functional support... yet. *show meme* (https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fi.redd.it%2F7t1p88ct13ez.jpg&f=1&nofb=1)
	that said, today's topic - loops, for loops, while loops, foreach loops,
	among other things that are kind of unique to Go like labels. so don't go
	anywhere :)

	 */

	// printing more than once
	fmt.Println("hello world")
	fmt.Println("hello world")
	fmt.Println("hello world")
	fmt.Println("hello world")
	fmt.Println()

	// Infinite loop
	sum := 0
	//for {
	fmt.Println(sum, "hello world")
	sum++ // repeated forever
	//}
	fmt.Println(sum) // never reached
	fmt.Println()

	// Three-component loop (typical for loop)
	sum = 0
	// for initialisation; condition; post {}
	for i := 1; i < 5; i++ {
		sum += i
	}
	fmt.Println(sum) // 10 (1+2+3+4)
	fmt.Println()

	// initialisation and post can be omitted
	i := 0
	for ;i <= 10; {
		fmt.Printf("%d ", i)
		i += 2
	}
	fmt.Println()
	fmt.Println()

	// semicolon too
	i = 0
	for i <= 10 {
		fmt.Printf("%d ", i)
		i += 2
	}
	fmt.Print()
	fmt.Println()

	// multiple variables initialisation and increment
	for no, i := 10, 1; i <= 10 && no <= 19; i, no = i + 1, no + 1 {
		fmt.Printf("%d * %d = %d\n", no, i, no*i)
	}
	fmt.Println()

	// While loop
	n := 1
	for n < 5 {
		n *= 2
	}
	fmt.Println(n) // 8 (1*2*2*2)
	fmt.Println()

	// For-each range loop
	str := "hello world"
	for count, c := range str {
		fmt.Printf("%d: %c", count, c)
		fmt.Println()
	}
	fmt.Println()

	// Exit a loop
	sum = 0
	for i := 1; i < 5; i++ {
		if i%2 != 0 { // skip odd numbers
			continue
			// break

		}
		sum += i
	}
	fmt.Println(sum) // 6 (2+4)
	fmt.Println()

	// Label
outer:
	for i := 0; i < 3; i++ {
		for j := 1; j < 4; j++ {
			fmt.Printf("i = %d , j = %d\n", i, j)
			if i == j {
				break outer
			}
		}

	}
	fmt.Println()

	// Nested loops
	for i := 0; i < 5; i++ {
		for j := 0; j <= i; j++ {
			fmt.Print("*")
		}
		fmt.Println()
	}
	fmt.Println()

}
