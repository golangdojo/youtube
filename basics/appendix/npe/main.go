package main

import "fmt"

type person struct {
	name string
}

func main() {
	// Title: Golang NPE - Null Pointer Exception in Go
	var p *person
	fmt.Println(changeName(p, "Wallace"))
}

func changeName(p *person, name string) *person {
	p.name = name
	return p
}
