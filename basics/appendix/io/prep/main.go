package main

import (
	"fmt"
	"io"
	"os"
)

func main() {
	// Package io provides basic interfaces to I/O(input/output) primitives
	// 1) Writer, Reader & Seeker interfaces
	// 2) Functions working with these interfaces

	// Writer interface
	file, _ := os.Create("file.txt")
	writer := io.Writer(file)
	n, err := writer.Write([]byte("Hello"))
	fmt.Printf("Write n={%v}, err={%v}\n", n, err)
	n, err = io.WriteString(writer, "!")
	fmt.Printf("WriteString n={%v}, err={%v}\n", n, err)
	file.Close()

	// Reader interface
	file, _ = os.Open("file.txt")
	reader := io.Reader(file)
	buffer := make([]byte, 10)
	n, err = reader.Read(buffer)
	fmt.Printf("Read n={%v}, err={%v}, buffer={%v}\n", n, err, string(buffer))
	file.Close()

	file, _ = os.Open("file.txt")
	reader = io.Reader(file)
	minBuffer := make([]byte, 1)
	for {
		n, err = reader.Read(minBuffer)
		fmt.Printf("%v, %v, %v\n", string(minBuffer), n, err)
		if err != nil || n == 0 {
			break
		}
	}
	file.Close()
	fmt.Println(io.EOF) // One of the few expected errors

	file, _ = os.Open("file.txt")
	reader = io.Reader(file)
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll buffer={%v}, err={%v}\n", string(buffer), err)
	file.Close()

	file, _ = os.Open("file.txt")
	reader = io.Reader(file)
	n, err = file.Read(buffer)
	fmt.Printf("ReadAll buffer={%v}, err={%v}\n", string(buffer), err)
	file.Close()

	// Seeker interface
	file, _ = os.Open("file.txt")
	reader = io.Reader(file)
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll  buffer={%v}, err={%v}\n", string(buffer), err)
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll  buffer={%v}, err={%v}\n", string(buffer), err)
	fmt.Println(file.Seek(0, io.SeekStart))
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll  buffer={%v}, err={%v}\n", string(buffer), err)
	fmt.Println(file.Seek(-5, io.SeekEnd))
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll  buffer={%v}, err={%v}\n", string(buffer), err)
	fmt.Println(file.Seek(-5, io.SeekCurrent))
	buffer, err = io.ReadAll(reader)
	fmt.Printf("ReadAll  buffer={%v}, err={%v}\n", string(buffer), err)
	file.Close()

	src, _ := os.Create("src.txt")
	io.Writer(src).Write([]byte("Hello!"))
	src.Close()
	src, _ = os.Open("src.txt")
	dst, _ := os.Create("dst.txt")
	fmt.Println(io.Copy(dst, src))
	src.Close()
	dst.Close()
}
