package main

import (
	"log"
	"os"
)

func main() {
	// Base logger
	log.Println("Sup Ninjas!")

	// Update attached info
	// Can't control order of flags
	log.SetFlags(log.Ldate | log.Lshortfile /*gives nice link*/)
	log.Println("Sup Ninjas!")

	// Unexpected problems
	//log.Panic("Panicking...") // can be recovered
	//log.Fatal("Uh-oh...")     // exits right now

	// Output a file
	file, _ := os.Create("file.txt")
	log.SetOutput(file)
	log.Println("Printing into a file")
	file.Close()
	log.SetOutput(os.Stdout)
	log.Println("Printing into standard out again")

	// Common loggers
	flags := log.LstdFlags | log.Lshortfile
	infoLogger := log.New(os.Stdout, "INFO: ", flags)
	warnLogger := log.New(os.Stdout, "WARN: ", flags)
	errorLogger := log.New(os.Stdout, "ERROR: ", flags)
	infoLogger.Println("This should be an info log")
	warnLogger.Println("This should be a warn log")
	errorLogger.Println("This should be an error log")
	// If this is logged into a file or a third party service,
	// you can filter or query by their prefixes.

	// You can also aggregate all three into one
	l := newAggregateLogger()
	l.info("Custom info")
	l.warn("Custom warn")
	l.error("Custom error")
}

type aggregateLogger struct {
	infoLogger  *log.Logger
	warnLogger  *log.Logger
	errorLogger *log.Logger
}

func newAggregateLogger() *aggregateLogger {
	flags := log.LstdFlags | log.Lshortfile
	return &aggregateLogger{
		infoLogger:  log.New(os.Stdout, "INFO: ", flags),
		warnLogger:  log.New(os.Stdout, "WARN: ", flags),
		errorLogger: log.New(os.Stdout, "ERROR: ", flags),
	}
}

func (l aggregateLogger) info(v ...interface{}) {
	l.infoLogger.Println(v...)
}

func (l aggregateLogger) warn(v ...interface{}) {
	l.warnLogger.Println(v...)
}

func (l aggregateLogger) error(v ...interface{}) {
	l.errorLogger.Println(v...)
}
