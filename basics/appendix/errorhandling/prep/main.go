package main

import (
	"errors"
	"fmt"
	"strconv"
)

//func returnError(returnError bool) error {
//	if returnError {
//		return errors.New("Error!")
//	} else {
//		return nil
//	}
//}

func returnError(returnError bool) (string, error) {
	if returnError {
		return "", errors.New("error here")
	} else {
		return "Random result", nil
	}
}

type specialError struct {
	errorMessage string
	errorCode    int
}

func (s specialError) Error() string {
	return fmt.Sprint(s.errorMessage, " ", strconv.Itoa(s.errorCode))
}

func main() {
	//err := returnError(true)
	//if err != nil {
	//	fmt.Println("Found error: ", err)
	//} else {
	//	fmt.Println("Didn't find error")
	//}

	result, err := returnError(true)
	if err != nil {
		fmt.Println("Found error!", err, result)
	} else {
		fmt.Println("Didn't find error", result)
	}

	// Look into error interface, any struct with Error implemented is of error type
	// This is the concept of duck typing, if it can...
	// In this case, if it implements all the methods in error interface, it's an error
	err = specialError{"Error message", 123}
	if err != nil {
		fmt.Println(err.Error())
		fmt.Println(err)
	}
}
