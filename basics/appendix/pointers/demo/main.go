package main

import "fmt"

func main() {
	wallace := "Uses a ninja star"
	fmt.Println(wallace) // Prints "Uses a ninja star"
	changeGear(wallace)
	fmt.Println(wallace) // Prints "Uses a ninja star"  (same)
	changeGearPointer(&wallace)
	fmt.Println(wallace)
}

func changeGear(wallaceCopy string)  {
	wallaceCopy = "Uses a ninja sword"
}

func changeGearPointer(wallacePointer *string)  {
	*wallacePointer = "Uses a ninja sword"
}