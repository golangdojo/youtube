package main

import "fmt"

func main() {
	fmt.Println("Hello 世界")

	wallace := "Uses a ninja star"
	fmt.Println(wallace)
	changeGear(wallace)
	fmt.Println(wallace)
	changeGearPointer(&wallace)
	fmt.Println(wallace)

}

func changeGear(gear string)  {
	gear = "Uses a ninja sword"
}

func changeGearPointer(gear *string)  {
	*gear = "Uses a ninja sword"
}
