package main

import (
	"fmt"
)

func main() {
	/*
	title: FULLY Understanding Golang Rune
	*/

	fmt.Println("Hello 世界")

	/*
	TL;DR - Runes are Characters in Go with native UTF-8 support. For

	(what is a character)
	when we write in the real world, sentences are made out of words
	words are made out of letters or characters
	when we are writing a program, not just in go but in any languages
	it's the same, except sentences are now called strings, but these
	strings are made out of individual characters

	For example, in a most basic program, a hello world program,
	we would print out a string made out of individual characters of
	"h-e-l-l-o- -w-o-r-l-d", and these characters are known as ascii
	characters

	(what is an ascii character)
	But what is an ascii character?
	it take a number from 0 to 127 to represent each one of common
	characters we use, as you can see from this table (http://www.asciitable.com/index/asciifull.gif)
	, and 127 is a number we can present with 7 binary bits.
	00000001 -> 1, 01111111 -> 127

	However, we had some problems here. ascii table is great, but only great for
	latin alphabet based languages which english uses, not so much for the rest of
	the world even for other European languages like french, german and many more.
	So people started using numbers 128-255 for extended character sets, which pushed
	the number of bits from 7 to 8, which is one full computer byte

	standards that use 8 bits like ISO-8859 became popularized, and for the most part
	they worked great. However, there were still problems. there are too many
	other languages like chinese for example that have way more characters than 255.
	as a result, there have been many "standard"'s in attempt to solve these problems

	(what is UTF-8)
	fast forward to present days, the most widely accepted encoding standard is UTF-8
	it's a flexible encoding that allows single byte and multiple byte characters to
	coexist in a single sentence or string.
	0xxxxxxx -> ascii, starting bits of 0 indicates it's a one byte UTF-8 character
	110xxxxx 10xxxxxx - 110 for two bytes, 10 for regular bits storage, leaves us 11 bits
	1110xxxx 10xxxxxx 10xxxxxx - starting bits as 1110, leaves us 16 bits
	11110xxx 10xxxxxx 10xxxxxx 10xxxxxx - 21 bits, 2,097,152 characters
	and that is the standard that Golang natively support

	(non UTF-8 in other languages)
	Now if you have any programming experience, when we are only working with ascii
	character, we typically use something called char to store these characters. Java
	would be one of these languages. As you can see from the example

	char a = 'a';
	System.out.println(a);
	// and strings
	String s = "Hello World";
	System.out.println(s);

	However, if we use any characters outside of the ascii table. The Java compiler is
	going to complain

	char a = '世';
	System.out.println(a);
	// and strings
	String s = "Hello 世界";
	System.out.println(s);

	In contrary to Go's native UTF-8 support
	a := '世'
	s := "世界"
	fmt.Println(a, s)

	(rune in go)
	However, how's that possible? '世' is a multi-byte character and char as we know it
	are supposed to be one byte?

	If we actually dig into it and try to print out the type of this character, it
	would actually tell you it's type int32 which occupies 4 bytes of memory space,
	which is how much space UTF-8 encoding requires, instead of char or any other single
	byte type. More so, int32 is an underlying type of what Go actually uses rune, and we
	can explicitly declare it as

	a := rune('世')

	instead. And if we dig into the implementation of rune, we can see it's an int32.

	Now you might be thinking, this implementation kind of defeats the purpose of UTF-8
	right?! We just talked about UTF-8 allows single byte characters like ascii's and multi
	byte characters like a chinese letter to coexist and potentially save memory space

	(UTF-8 in Golang)
	To answer that question, let's take fmt.Println("Hello 世界") as an example, which
	happens to be the exact program golang.org has on its front page. "H-e-l-l-o- "
	are all characters from the ascii table. "世界" obviously are not. If we try to print
	out the len("H"), we can have a peak of len()'s implement, it should tell us byte
	size of the given string, which is one in this case, which make sense from all that
	we've tried to explain in this video. if we try to print out len("世“), it's a 3 byte
	character instead, which is the byte size required to represent a chinese character
	like "世". On top of that, Go has native support for you to iterate through the string
	by UTF-8 character with Go's foreach loop

	for index, runeValue := range str {
		fmt.Println(index, string(runeValue))
	}

	So yes, using rune may or may not be the most memory efficient, but we can use memory
	efficient strings instead when writing anything more than individual characters.
	OR we can use byte b := byte('c') if we know for a fact that we are only operating
	on one byte characters and memory optimization is a big deal in your particular use case.
	Just note that a := 'a' isn't going to give that by default but a rune instead.

	Hope this lengthy explanation didn't bore too much. For more details on how to iterate
	over UTF-8 strings, and etc., subscribe to the channel for future dedicated videos on
	these topics


	*/

	fmt.Printf("%T", 'a')
	fmt.Println()


}
