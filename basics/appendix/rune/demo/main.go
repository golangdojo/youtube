package main

import "fmt"

func main() {
	c := rune('世')
	fmt.Println(c)
	fmt.Printf("%T", c)
	fmt.Println()

	s := "Hello 世界"
	fmt.Println(s)
	fmt.Println(len(s))
	fmt.Println(len("世界"))
	c = 'c'
	b := byte(65)
	fmt.Printf("%c", b)
	fmt.Println()
	fmt.Printf("%T", b)
}
