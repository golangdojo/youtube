package main

import "fmt"

func main() {

	venues := []string{"Home", "Work", "School", "Bar", "Gym"}

	for _, venue := range venues {
		switch venue {
		case "Home":
			greeting("Mom, I'm hungry")
		case "Work", "School":
			greeting("Weather is great today")
		case "Bar":
			greeting("Hey I like your dress, but it's a little too blue")
		default:
			greeting("Sup bois")
		}
	}

}

func greeting(greeting string) {
	fmt.Println(greeting)
}
