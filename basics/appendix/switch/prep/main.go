package main

import "fmt"

func main() {

	venues := []string{"Home", "Work", "School", "Bar", "Gym"}

	for _, venue := range venues {
		if venue == "Home" {
			greeting("Mom, I'm hungry")
		} else if  venue == "Work" || venue == "School" {
			greeting("Weather is great today")
		} else if  venue == "Bar" {
			greeting("Hey I like your dress, but it's a little too blue")
		} else {
			greeting("Sup boi")
		}
	}

	for _, venue := range venues {
		switch venue {
		case "Home":
			greeting("Mom, I'm hungry")
		case "Work", "School":
			greeting("Weather is great today") // awkward water cooler talks
		case "Bar":
			greeting("Hey I like your dress, but it's a little too blue")
		default:
			greeting("Sup boi")
		}
	}
}

func greeting(greeting string) {
	fmt.Println(greeting)
}
