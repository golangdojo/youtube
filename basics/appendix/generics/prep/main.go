package main

import (
	"fmt"
	"golang.org/x/exp/constraints"
)

func main() {
	fmt.Println(min(1, 2))
	s := set[int]{}
	s.contains(1)
	s.add(1)
	s.contains(1)
	s.remove(1)
	s.contains(1)
}

func minInt(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func minFloat64(a float64, b float64) float64 {
	if a < b {
		return a
	}
	return b
}

func minString(a string, b string) string {
	if a < b {
		return a
	}
	return b
}

//func min[any](a any, b any) any {
//	return a + b
//}
//
//func min[T any](a T, b T) T {
//	return a + b
//}

type minTypes interface {
	int | float64 | string
}

//type myString string
//var ms myString = "myString"
//min("string", ms)
//func min[T minTypes](a T, b T) T {
//	if a < b {
//		return a
//	}
//	return b
//}

func min[T constraints.Ordered](a T, b T) T {
	if a < b {
		return a
	}
	return b
}

type set[T constraints.Ordered] struct {
	m map[T]struct{}
}

func (s *set[T]) contains(v T) bool {
	_, ok := s.m[v]
	return ok
}

func (s *set[T]) add(v T) {
	s.m[v] = struct{}{}
}

func (s *set[T]) remove(v T) bool {
	_, ok := s.m[v]
	if !ok {
		return false
	}
	delete(s.m, v)
	return true
}
