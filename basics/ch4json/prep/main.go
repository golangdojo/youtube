package main

import (
	"encoding/json"
	"fmt"
)

type ninja struct {
	// capitalize to be used in for json
	// can be omitted if same name
	Name   string `json:"name"`
	Weapon string `json:"weapon"`
	Level  int    `json:"level"`
}

type skilledNinja struct {
	Name   string `json:"name"`
	Weapons []string `json:"weapons"`
	Level  int    `json:"level"`
}

func main() {
	fmt.Println("Hello 世界")

	sFrom := `{"name": "Wallace", "weapon": "Ninja Star", "level": 1}`
	var wallace ninja
	err := json.Unmarshal([]byte(sFrom), &wallace)
	if err != nil {
		fmt.Println("Sad boi")
	}
	fmt.Println(wallace)

	sTo, err := json.Marshal(wallace)
	if err != nil {
		fmt.Println("Sad boi again")
	}
	fmt.Printf("%s", sTo)
	fmt.Println()
	fmt.Println()

	sFrom = `{"name": "Wallace", "weapons": ["Ninja Star", "Ninja Sword"], "level": 1}`
	var skilledWallace skilledNinja
	err = json.Unmarshal([]byte(sFrom), &skilledWallace)
	if err != nil {
		fmt.Println("Sad boi")
	}
	fmt.Println(skilledWallace)

	sTo, err = json.Marshal(skilledWallace)
	if err != nil {
		fmt.Println("Sad boi again")
	}
	fmt.Printf("%s", sTo)
	fmt.Println()

}
