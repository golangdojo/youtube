package main

import (
	"encoding/json"
	"fmt"
)

type Ninja struct {
	Name   string `json:"full_name"`
	Weapon Weapon `json:"weapon"`
	Level  int `json:"level"`
}

type Weapon struct {
	Name string `json:"weapon_name"`
	Level int `json:"weapon_level"`
}

func main() {
	fmt.Println("Hello 世界")


	// from json object to go object
	sFrom := `{"full_name": "Wallace", "weapon": {"weapon_name":"Ninja Star", "weapon_level":1}, "level": 1}`
	fmt.Println(sFrom)
	var wallace Ninja
	fmt.Println(wallace)
	err := json.Unmarshal([]byte(sFrom), &wallace)
	if err != nil {
		fmt.Println("Sad boi")
		fmt.Println(err)
	}
	fmt.Println(wallace)

	// from go object to json object
	sTo, err := json.Marshal(wallace)
	if err != nil {
		fmt.Println("Sad boi again")
	}
	fmt.Println(sTo)
	fmt.Printf("%T\n", sTo)
	fmt.Printf("%s\n", sTo)
}
