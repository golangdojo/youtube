package main

import "fmt"

func main() {
	fmt.Println("Hello 世界")

	type ninja struct {
		name    string
		weapons []string
		level   int
	}

	wallace := ninja{name: "Wallace"}
	wallace = ninja{"Wallace", []string{"Ninja Star"}, 1}
	fmt.Println(wallace)
	fmt.Println(wallace.name)
	fmt.Println(wallace.weapons)
	fmt.Println(wallace.level)
	wallace.level++
	wallace.weapons = append(wallace.weapons, "Ninja Sword")
	fmt.Println(wallace)

	type dojo struct {
		name  string
		ninja ninja
	}

	golangDojo := dojo{
		name:  "Golang Dojo",
		ninja: wallace,
	}
	fmt.Println(golangDojo)
	fmt.Println(golangDojo.ninja.level)
	golangDojo.ninja.level = 3
	fmt.Println(golangDojo.ninja.level)
	fmt.Println(wallace.level)

	type addressedDojo struct {
		name  string
		ninja *ninja
	}
	addressed := addressedDojo{"Addressed Golang Dojo", &wallace}
	fmt.Println(addressed)
	fmt.Println(*addressed.ninja)
	addressed.ninja.level = 4
	fmt.Println(addressed.ninja.level)
	fmt.Println(wallace.level)

	jonnyPointer := new(ninja)
	fmt.Println(jonnyPointer)
	fmt.Println(*jonnyPointer)
	jonnyPointer.name = "Jonny"
	jonnyPointer.weapons = []string{"Ninja Star"}
	jonnyPointer.level = 1
	fmt.Println(*jonnyPointer)

	intern := ninjaIntern{name: "Intern", level: 1}
	intern.sayHello()
	intern.sayName()

}

type ninjaIntern struct {
	name string
	level int
}

func (ninjaIntern) sayHello() {
	fmt.Println("Hello")
}

func (n ninjaIntern) sayName() {
	fmt.Println(n.name)
}

