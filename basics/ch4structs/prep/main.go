package main

import (
	"fmt"
	"reflect"
)

func main() {

	fmt.Println("Hello 世界")

	// have to get used to structs/classes not being capitalized
	type ninja struct {
		// Name if to be exported
		name    string
		weapons []string
		level   int
	}

	// let's say Wallace is trying to become a Ninja
	wallace := ninja{name: "Wallace"}
	wallace = ninja{
		name:    "Wallace",
		weapons: []string{"Ninja Star"}, // check last vid
		level:   1,
	}
	fmt.Println(wallace)
	wallace = ninja{"Wallace", []string{"Ninja Star"}, 1}
	fmt.Println(wallace)
	wallace.level++
	wallace.weapons = append(wallace.weapons, " Ninja Sword")
	fmt.Println(wallace)

	type dojo struct {
		name string
		//ninjas []ninja
		ninja ninja // name can be omitted if only one
	}

	golangDojo := dojo{
		name:  "Golang Dojo",
		ninja: wallace, // can still use ninja as a placeholder
	}
	fmt.Println(golangDojo)

	golangDojo.ninja.level = 3
	// golangDojo.level = 3 with ninja name omitted
	fmt.Println(golangDojo)

	fmt.Println(wallace)

	type addressedDojo struct {
		name string
		//ninjas []ninja
		ninja *ninja // name can be omitted if only one
	}

	addressed := addressedDojo{
		name:  "Golang Dojo",
		ninja: &wallace, // can still use ninja as a placeholder
	}
	fmt.Println(addressed)
	addressed.ninja.level = 9001
	fmt.Println(wallace)

	// same thing applies on the entire object of addressed dojo
	addressedCopy := addressed
	addressedCopy.name = "Addressed Copy"
	fmt.Println(addressed)
	addressedPointer := &addressed
	addressedPointer.name = "Addressed Pointer"
	fmt.Println(addressed)

	// using new
	addressedPointer = new(addressedDojo)
	addressedPointer.name = "Golang Dojo"
	fmt.Println(addressedPointer)
	// but reassigning with *
	*addressedPointer = addressedDojo{}
	// addressedPointer = addressedDojo{} // error
	fmt.Println(addressedPointer)

	// more on pointers, check out my last video

	// use struct{} as a hashset with map[*]struct{}
	set := map[string]struct{}{}
	set["wallace"] = struct{}{}
	fmt.Println(set)
	// compare to slice, map, map with bool

	// there are other ways to use free structs such as for channels
	// sub


	// comparing struct
	type point struct {
		x, y int
	}
	fmt.Println(point{1, 2} == point{1, 2})
	fmt.Println(point{1, 2} == point{2, 2})
	fmt.Println(reflect.DeepEqual(ninja{}, ninja{}))
	// more on reflection on future videos

	// move ninja struct, adding function to a struct, pretty different
	// from a typical object oriented programming language
	// we will talk more about functions in future videos as well
	intern := ninjaIntern{"Intern", 1}
	fmt.Println(intern)
	intern.sayHello()
	intern.sayName()

}

type ninjaIntern struct {
	name string
	level int
}

func (*ninjaIntern) sayHello() {
	fmt.Println("Hello")
}

func (nPointer *ninjaIntern) sayName() {
	fmt.Println(nPointer.name)
}