package main

import (
	"fmt"
	_ "golangdojo.com/golangdojo/youtube/basics/ch10blankimports/prep/sideeffect"
)

func main() {
	fmt.Println("Hello World")

	// slow initialization
	// https://stackoverflow.com/questions/54167313/what-does-import-lib-math-in-golang?noredirect=1&lq=1
}
