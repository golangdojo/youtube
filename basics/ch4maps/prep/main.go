package main

import "fmt"

func main() {
	fmt.Println("Hello 世界")

	/*

	maps are super useful, especially in a coding interview
	when you are stunk and pulling your hair in front of the interviewer
	just somehow figure out a way to use a map
	and there's a 50/50 chance you'd be right

	that said, let's take a look at how to use a map in go today
	 */

	var m map[string]string
	fmt.Println(m == nil)
	m = map[string]string{}
	fmt.Println(m == nil)
	fmt.Println(m)
	fmt.Println(len(m))
	m = make(map[string]string, 5)
	fmt.Println(m == nil)
	fmt.Println(m)
	m = map[string]string{"Wallace":"Programmer"}
	fmt.Println(m)
	fmt.Println(len(m))
	m["John"] = "Not a programmer"
	fmt.Println(m)
	m["John"] = "Programmer to be"
	fmt.Println(m) // not ordered
	fmt.Println(m["Wallace"])
	fmt.Println()

	delete(m, "John")
	fmt.Println(m)

	m["Wallace"] += " YouTuber"
	fmt.Println(m)

	for name, title := range m {
		fmt.Println(name, title)
	}

	title, ok := m["John"]
	// title, ok := m["Wallace"]
	if ok {
		fmt.Println(title)
	} else {
		fmt.Println("Didn't find John")
	}

	title = m["John"]
	title, _ = m["John"]




}
