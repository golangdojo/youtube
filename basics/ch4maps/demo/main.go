package main

import "fmt"

func main() {
	var m map[string]string
	fmt.Println(m == nil)
	m = map[string]string{}
	fmt.Println(m == nil)
	fmt.Println(m)
	fmt.Println(len(m))
	m = make(map[string]string, 5)
	fmt.Println(len(m))
	m = map[string]string{"Wallace": "Programmer"}
	fmt.Println(m)
	fmt.Println(len(m))
	m["Johnny"] = "Not a programmer"
	fmt.Println(m)
	m["Johnny"] = "Programmer to be"
	fmt.Println(m)
	fmt.Println(m["Johnny"])

	delete(m, "Johnny")
	fmt.Println(m)
	m["Wallace"] += " Ninja"
	fmt.Println(m)

	for name, title := range m {
		fmt.Println(name, title)
	}

	fmt.Println(m["Johnny"])
	m["Johnny"] = "Programmer to be"

	title, ok := m["Johnny"]
	if ok {
		fmt.Println(title)
	} else {
		fmt.Println("Didn't find Johnny")
	}
	title, _ = m["Johnny"]
	title = m["Johnny"]
}
