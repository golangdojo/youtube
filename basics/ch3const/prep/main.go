package main

import (
	"fmt"
	"math"
	"math/big"
	"time"
)

func main() {
	fmt.Println("Hello 世界")

	/*
	constants are everywhere in any sizable programming project, and Golang
	projects are no exception. However, constants in go are implemented quite
	differently compared to other programming languages. And that's today's
	topic so you can use them correctly
	 */

	// *** title: Learn Golang in [] Minutes - Constants & Unsigned Constants

	/*
	definition
	a constant is an expression whose value is known to the compiler
	and whose evaluation is guaranteed to occur at compile time, not run time
	in other word, a value determined on compile time and will not change
	 */
	const five = 5
	fmt.Println(five)
	// one = 3

	// some values are harder to write than others
	// and we don't want to write them more than ones
	const pi = 3.14
	fmt.Println(pi)
	fmt.Println(math.Pi)

	const (
		a = 3
		b
		c = 2
		d
	)
	fmt.Println(a, b, c, d)

	// iota smallest letter of its
	const (
		zero int = iota
		one
		two
	)
	fmt.Println(zero, one, two)

	// more interesting ones
	const (
		_ = 10 * iota // _ means we don't care
		ten
		hundred
		thousand
	)
	fmt.Println(ten, hundred, thousand)

	// slightly more complicated ones
	const (
		_  = 1 << (10 * iota) // two to the power of 10, 2 ^ 10
		kb
		mb
		gb
		tb
		pb
		eb
		zb
		yb
	)
	fmt.Println(kb, mb, gb)

	// untyped const
	// fmt.Println(yb) // compiler assumes int
	// overflows because of default but you can still use it in other ways
	// constant.Value() // slice/array of uint's in math/big
	fmt.Println(yb / zb)

	// const ybTyped = big.Int{}
	// but you can't easily because constants can't be variables
	// being assigned a value by a function like math.Pow()
	// const notOneVariable = math.Pow(100, 100) // won't work

	// for the same reason, there are limitations of iota
	// because there are limitations of const
	// you can't always express what you want without variable functions

	// common constants we can utilize that are built-in
	fmt.Println(math.Pi)
	fmt.Println(time.February)
	fmt.Println(time.Second)
	fmt.Println(time.UTC)
	fmt.Println(big.MaxExp)

}
