package main

import "fmt"

func main() {
	defer safeExit()

	const one = 2
	if one != 1 {
		panic("one isn't 1")
	}
	// haven't work in production code,
	// not sure the legitimate cases
	// to panic instead of returning an error
	// or logging an error
}

func safeExit() {
	if r := recover(); r != nil {
		fmt.Println("Recovered!")
	}
}
