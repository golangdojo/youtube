package main

import "fmt"

func main() {
	fmt.Println("Hello 世界")

	fmt.Printf("%T\n", returnParam)
	fmt.Printf("%T\n", returnParam(1))
	fmt.Printf("%d\n", returnFunction(1, returnParam))

	// defer first()
	second()
	second()

	aFunction := func() {
		fmt.Println("aFunction")
	}
	fmt.Println("Hello World")
	aFunction()
	func() {
		fmt.Println("aFunction 2")
	}()

	aFunction = first
	aFunction()

	crazyishFunction := func(f func()) func() {
		return f
	}

	crazyFunction(crazyishFunction)(first)()
}

func crazyFunction(f func(func()) func()) func(func()) func() {
	return f
}

func first() {
	fmt.Println("first")
}

func second() {
	fmt.Println("second")
}

func returnParam(i int) int {
	return i
}

func returnFunction(i int, f func(int) int) int {
	return f(i)
}

