package main

import "fmt"

func main() {
	fmt.Println("Hello 世界")


	// functions are first class values, they are their own data type
	fmt.Printf("%T\n", returnParam)
	fmt.Printf("%T\n", returnParam(1))

	// pass a function as a parameter
	fmt.Println(withFunction(1, returnParam))

	// deferred function calls
	// defer second()
	first()
	// good for things like closing files and timing latency
	// will defer to the end
	fmt.Println("hi")

	// anonymous functions
	anonymousFunction := func() {
		fmt.Println("anonymousFunction")
	}
	fmt.Println("main")
	anonymousFunction()

	// reassign it to a pre-defined function
	anonymousFunction = first
	anonymousFunction()

	// as a return type
	fmt.Printf("%T\n", returnFunction)
	returnFunction()()

	// closure
	closure := func(i int) int {
		fmt.Println("Closure has: ", i)
		return i
	}(1)
	// 1 is the default parameter
	fmt.Println(closure)

	crazyishFunction := func(f func()) func() {
		return f
	}

	crazyFunction(crazyishFunction)(first)()
}

func crazyFunction(f func(func()) func()) func(func()) func() {
	return f
}

func returnParam(i int) int {
	return i
}

func withFunction(i int, f func(int) int) int {
	return f(i)
}

func returnFunction() func() {
	return func() {
		fmt.Println("Returned function")
	}
}

func first() {
	fmt.Println("First")
}
func second() {
	fmt.Println("Second")
}