package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello 世界")

	// you wouldn't want to keep creating variables 100 times
	// var a, b, c, d... = 1, 2, 3, 4...
	// also arrays in Go are pretty different from other
	// c like languages such as c, c++ & java.
	// Keep on watching if you want to learn more

	var a [3]int
	fmt.Println(a[0])
	// fmt.Println(a[4]) // error
	fmt.Println(a)
	fmt.Println(len(a))
	fmt.Println()

	// arrays are values instead of addresses
	a[0] = 1
	aCopy := a
	fmt.Println(a == aCopy)
	aCopy[0] = 2
	fmt.Println(a, aCopy)
	fmt.Println(a == aCopy)

	// unlike what we talked about strings, arrays are mutable
	a[0] = 1
	fmt.Println(a[0])
	fmt.Println()

	// assign values at declaration
	b := [4]int{1, 2, 3, 4}
	fmt.Println(b)
	c := [...]int{1, 2}
	fmt.Println(c)
	d := [3]int{1, 2}
	fmt.Println(d)
	e := [...]int{1, 2, 0}
	fmt.Println(d == e)
	//fmt.Println(b == e) // can't compare different types

	// can't reassign with a different size/type
	// a = [4]int // error
	fmt.Printf("%T", a)
	fmt.Println()
	// fmt.Printf("%T", [4]int(a)) // can't cast either
	// c = [4]int // even with ones initialized with [...]int

	// you can only cast using the same size, but that gets ignored by the compiler
	fmt.Println([3]int(a))
	fmt.Println()

	// for v:= range b {
	// for i, v := range b {
	for _, v := range b {
		fmt.Println(v)
	}
	fmt.Println()

	array := [...]int{0: 1}
	fmt.Println(array)
	array2 := [...]int{1: 1, 4: 5}
	fmt.Println(array2)
	fmt.Println()

	// i've only talked about int arrays so far,
	// but everything holds true for other types of arrarys too
	strArray := [...]string{"string1", "string2"}
	fmt.Println(strArray)
	fmt.Println()

	// note that we need ..., else it becomes a slice
	// allocated on the heap
	// instead of an array instantialized on the stack
	// subscribe for our next lesson on slices and the differences
	strSlice := []string{"string1", "string2"}
	fmt.Printf("%T, %T", strArray, strSlice)
	fmt.Println()
	fmt.Println()

	// 2d arrays
	array2d := [2][2]int{{1, 2}, {3, 4}}
	fmt.Println(array2d)
	array3d := [2][2][2]int{array2d, array2d}
	fmt.Println(array3d)

}
