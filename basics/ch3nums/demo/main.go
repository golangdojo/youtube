package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("Hello 世界")

	var x = 1 // int32 int64
	var x32 int32 = 1
	var x64 int64 = 1
	fmt.Println(x)
	fmt.Println(x32)
	fmt.Println(x64)

	fmt.Printf("%T, %T, %T", x, x32, x64)
	fmt.Println()

	x = int(x32)
	fmt.Println(x)
	x = int(x64)
	fmt.Println(x)

	var unsigned uint = 1
	fmt.Println(unsigned)

	y := 2
	fmt.Println(x + y) // 3
	fmt.Println(x % y) // 1
	fmt.Println(x == y) // false
	fmt.Println(x < y) // true

	// floating point numbers

	pi := 3.141
	fmt.Println(pi)
	fmt.Printf("%T", pi)
	fmt.Println()

	var pi32 float32 = 3.141
	fmt.Printf("%T", pi32)

	pi = float64(3.141)
	fmt.Println(pi)

	ninjas := 1e100
	fmt.Println(ninjas)

	// conversion between the two
	a := 1
	b := 3.99999
	fmt.Println(a)
	fmt.Println(b)
	a = int(b)
	fmt.Println(a)

	fmt.Println(math.Ceil(3.0000001)) // 4
	fmt.Println(math.Floor(3.0000001)) // 3
	fmt.Println(math.Min(1, 0)) // 0
	fmt.Println(math.Max(1, 0)) // 1
	fmt.Println(math.Abs(-1)) // 1
	fmt.Println(math.Sqrt(100)) // 10
	fmt.Println(math.Pow(2, 3)) // 8

	// ** bonus **
	// complex numbers
	fmt.Println(complex(1, 2))

}