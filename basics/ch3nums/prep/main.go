package main

import (
	"fmt"
	"math"
)

func main() {
	fmt.Println("hello world")

	/*
	Today we are talking about numbers and the math library in Go.
	For numbers we will go over integer, floating point number and maybe touch
	on complex numbers.
	For math library, we will explore what the Go standard library has to offer
	Especially in a coding interview, the math library often can save you from
	tremendous headaches under tight time constrain. Alright, let's get started
	 */

	// ** integer **
	var x int = 1
	fmt.Println(x)
	// check declaration video

	// defaults to int32 or int64 depending on the machine
	y := 1
	fmt.Printf("%T", y)
	fmt.Println()

	// you still can't convert directly
	var y32 int32 = 1
	fmt.Printf("%T", y32)
	var y64 int64 = 1
	fmt.Printf("%T", y64)
	x = int(y32)
	x = int(y64)

	// comment if you want me to go over int of more different bit sizes,
	// signed & unsigned, or more about bitwise manipulation

	// aside from that, of course you can do all the typical math operations
	fmt.Println(x + y)
	fmt.Println(x % y)
	fmt.Println(x == y)
	fmt.Println(x < y)


	// ** floating point number **

	// defaults to float64
	pi := 3.141
	fmt.Print("%T", pi)
	fmt.Println()

	// scientific notation
	ninjas := 1e100
	// whatever this mean, let's just say that i'm not very scientific
	fmt.Println(ninjas)

	// ** floating point, integer conversion **
	a := 1
	var b float64 = float64(a)
	fmt.Println(a)
	fmt.Println(b)
	var c float64 = 3.141
	var d int = int(c)
	fmt.Println(c)
	fmt.Println(d)
	var e float64 = 3.99
	var f int = int(e)
	fmt.Println(e)
	fmt.Println(f)

	// ** math library **
	fmt.Println(math.Ceil(3.3343))
	fmt.Println(math.Min(1, 0))
	fmt.Println(math.Max(1, 0))
	fmt.Println(math.Abs(1))
	fmt.Println(math.Sqrt(100))
	fmt.Println(math.Pow(2, 3))
	fmt.Println(math.Ceil(3.444))
	fmt.Println(math.Floor(3.444))


	// ** bonus **
	// there's complex numbers
	fmt.Println(complex(1, 2))
	// 1 + 2i, and please don't ask me what this is
	// just that I passed all of my classes in school doesn't mean I learned things

}
