package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	/*
	Today we are talking about strings, string builders,
	the strings library, in Go
	These are basic building blocks of any programming projects
	So don't go anywhere
	 */

	s := "Hello World"
	fmt.Println(s)
	fmt.Println(s)
	fmt.Println(len(s))
	fmt.Println(s[0])
	fmt.Println(s[:3])
	fmt.Println(s[1:3])
	fmt.Println(s[4:])

	s = s + " Again"
	s += " Again"
	fmt.Println(s)

	// your usual string literals
	fmt.Println("Hello\nWorld")
	fmt.Println("Hello\tWorld")
	fmt.Println("Hello\bWorld")
	fmt.Println("Hello\rWorld")

	// strings are slice of bytes, interchangle
	abc := "abc"
	b := []byte(abc)
	abc2 := string(b)
	fmt.Println(abc2)
	// more about slices coming up soon, it's unique data structure in Go

	// UTF-8 encoding for non-ascii charactors
	fmt.Println(len("世界"))
	fmt.Println("世界"[0])
	fmt.Println("世界"[0:3])
	fmt.Println("世界"[0:4])
	// if you're watching, english speaker
	// leave a comment for more UTF-8
	// and potentially Chinese specifically

	// string() with ascii values or hex values
	fmt.Println(string(65))
	fmt.Println(string(0x4e16), string(0x754c))

	// strings basics
	fmt.Println(strings.ToUpper("Hello World"))
	fmt.Println(strings.ToLower("Hello World"))
	fmt.Println(strings.HasPrefix("Hello World", "Hello"))
	fmt.Println(strings.HasSuffix("Hello World", "Hello"))
	fmt.Println(strings.HasSuffix("Hello World", "World"))
	fmt.Println(strings.Replace("Hello Word", "Word", "World", 1))

	// Only read works. Assignment won't work
	// s[0] = 'L'

	// Use string builder
	var sb strings.Builder
	sb.WriteString("Hello")
	fmt.Println(sb.String())
	fmt.Println(sb.Cap())
	fmt.Println(sb.Len())

	sb.Grow(100) // meh
	fmt.Println(sb.Cap()) // double the original first
	fmt.Println(sb.Len())
	fmt.Println(sb.String())

	sb.Reset()
	fmt.Println(sb.String())
	fmt.Println(sb.Cap())
	fmt.Println(sb.Len())

	sb.Write([]byte{65, 65, 65})
	fmt.Println(sb.String())

	// strings <-> numbers
	x := 123
	y := strconv.Itoa(x)
	fmt.Println(y)
	z, _ := strconv.Atoi(y)
	fmt.Println(z)
	// sub for error handling
}
