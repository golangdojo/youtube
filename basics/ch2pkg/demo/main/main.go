package main

import (
	"fmt"
	"golangdojo.com/golangdojo/youtube/basics/ch2pkg/demo/integers"

	//"golangdojo.com/golangdojo/bookgpl/ch2pkg/demo/integers"
	// host/userororganization/project/(dir)/package
)

var five = 5

func main() {

	{
		var integer = 2
		fmt.Println(integer)
	}

	var integer = 1

	fmt.Println("Hello 世界")
	fmt.Println(integer)
	fmt.Println(integers.Three)
}

func nonMain() {
	fmt.Println(five)
}

// block -> package -> universe