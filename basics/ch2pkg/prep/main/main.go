package main

import (
	"fmt"
	"golangdojo.com/golangdojo/youtube/basics/ch2pkg/prep/integers"

	///* ~/Users/Wallace/go/src/ */ "golangdojo.com/golangdojo/bookgpl/ch2pkg/prep/integers"
	// /* ~/Users/Wallace/go/src/ */ "golangdojo.com/golangdojo/bookgpl/ch2pkg/integers_test"
)

func main() {
	fmt.Println(one)
	// fmt.Println(integers.one)
	fmt.Println(integers.One)
	// fmt.Println(integers_test.NotGonnaWork)
}


/*


Today's video is all about packages in go. Go is very opinionate when it comes to package and file management. So if you've ever been confused and want to figure out how to hashmap up your prjects correctly. keep on watching


** scope **
What is scope
with the assumption that we know what a variable is
Scope of a variable is where that variable can be called or used

block -> package -> universe, go doesn't care about files

best practice is keeping the scope as narrow as possible when coding

"golangdojo.com/golangdojo/bookgpl/ch2pkg/strings"
"host/organization/project/(dir)/package"

** package and file structure **

* During Go installation *
GOPATH: C:\Users\Wallace\go
Created bin, pkg, src
bin 
pkg
src

creating projects in src

no cross reference

** exceptions **
test files
you can't export in test files
_test.go
var (
	N1 = 1
	N2 = 2
)

go test ./...

Also there are different ways to structure your test packages depending on the type of testing 
Such as blackbox vs whitebox
We will talk about testing in a separate video


*/