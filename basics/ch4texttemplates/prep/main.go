package main

import (
	"fmt"
	"os"
	"text/template"
)

type secret struct {
	// capitalized to be used, for more, check out my scope video
	Name   string
	Secret string
}

func main() {
	secret := secret{"Wallace", "I'm a ninja"}
	templatePath := "C:/Users/Wallace/workspace/src/golangdojo.com/golangdojo/youtube/bookgpl/ch4texttemplates/prep/template.txt"
	t, err := template.New("template.txt").ParseFiles(templatePath)
	if err != nil {
		fmt.Println(err)
	}
	err = t.Execute(os.Stdout, secret)
	if err != nil {
		fmt.Println(err)
	}
}
