package main

import (
	"fmt"
	"os"
	"text/template"
)

type secret struct {
	Name string
	Secret string
}

func main() {

	// https://golang.org/pkg/text/template/

	fmt.Println("Hello 世界")

	secret := secret{"Wallace", "I'm a ninja"}

	templatePath := "C:/Users/Wallace/workspace/src/golangdojo.com/golangdojo/youtube/basics/ch4texttemplates/demo/template.txt"

	t, err := template.New("template.txt").ParseFiles(templatePath)
	if err != nil {
		fmt.Println(err)
	}

	err = t.Execute(os.Stdout, secret)
	if err != nil {
		fmt.Println(err)
	}

}
