package main

import "fmt"

type ninjaStar struct {
	owner string
}

type ninjaSword struct {
	owner string
}

func (ninjaStar) attack() {
	fmt.Println("Throwing ninja star")
}

func (ninjaSword) attack() {
	fmt.Println("Swinging ninja sword")
}

type ninjaWeapon interface {
	attack()
}

func attack(weapon ninjaWeapon) {
	weapon.attack()
}

// we can try implementing ninjaWeapon, which you can't because it's third party
// we can try inheritance then implement, which requires you to change all existing
// type references. On top of that, go despises inheritance
type thirdPartyWeapon struct {
	owner        string
	thirdPartyId string
	level        int
}

func (thirdPartyWeapon) attack() {
	fmt.Println("Using third party weapon")
}

func main() {

	// go over some examples of how to use interface in go
	// then comparisons with more traditional OOP languages like java
	// and explain why interface is done the way it is in go

	stars := []ninjaStar{{"Wallace"}, {"Wallace"}}
	for _, star := range stars {
		star.attack()
	}
	fmt.Println()

	swords := []ninjaSword{{"Wallace"}, {"Wallace"}}
	for _, sword := range swords {
		sword.attack()
	}
	fmt.Println()

	// what if you don't care if they are stars or swords, but as weapons in general

	weapons := []ninjaWeapon{ninjaStar{"Wallace"}, ninjaSword{"Wallace"}}
	for _, weapon := range weapons {
		attack(weapon)
	}
	fmt.Println()

	// biggest pro compared to a more traditional OOP language like java is
	// it is satisfied implicitly by existing types, esp. useful for others' packages
	weapons = []ninjaWeapon{
		ninjaStar{"Wallace"},
		ninjaSword{"Wallace"},
		thirdPartyWeapon{"Wallace", "Id", 1},
	}
	for _, weapon := range weapons {
		attack(weapon)
	}
	fmt.Println()

	// interface combo
	editors := []editor{siteEditor{"Wallace"}}
	for _, editor := range editors {
		edit(editor)
	}
}

type reader interface {
	read()
}

type updater interface {
	update()
}

type editor interface {
	reader
	updater
}

type siteEditor struct {
	name string
}

func (siteEditor) read() {
	fmt.Println("Reading")
}

func (siteEditor) update() {
	fmt.Println("Updating")
}

func edit(e editor) {
	e.read()
	e.update()
}
