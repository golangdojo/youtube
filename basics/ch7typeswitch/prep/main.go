package main

import "fmt"

type ninjaStar struct {
	owner string
}

type ninjaSword struct {
	owner string
}

func (ninjaStar) attack() {
	fmt.Println("Throwing ninja star")
}

func (ninjaStar) pickup() {
	fmt.Println("Picking up ninja star")
}

func (ninjaSword) attack() {
	fmt.Println("Swinging ninja sword")
}

type ninjaWeapon interface {
	attack()
}

func main() {
	weapons := []ninjaWeapon{ninjaStar{"Wallace"}, ninjaSword{"Wallace"}}

	for _, weapon := range weapons {
		weapon.attack()
		switch weapon.(type) {
		case ninjaStar:
			ns := weapon.(ninjaStar)
			ns.pickup()
		case ninjaSword:
			fmt.Println("No need to pick it back up")
		}
		// if else alternatives won't look nearly as nice
	}
}
