package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello 世界")

	/* slices are similar to but still quite different from array
	can think of array are already built houses,
	people who come and go, but the size of the house is fixed
	slices are a giant piece land offered to you for sales,
	notice that you don't own yet, but it's up for sales,
	for the most part you can choose at will how many of lots of land
	you choose to buy, and to build however many houses you want,
	could be one, two, ten, or even a whole town of houses

	More technically speaking, array are fixed sized amount of elements you allocate
	on the stack, and slices are a potentially growing amount of elements you can
	allocate in the heap, those of you that don't understand stack and heap, leave a comment

	If I have to give you a comparable, it would be arraylists in java
	That said, let's go over how slices look like in go and we can use them
	 */

	// declaring slices
	a := []int{0, 1, 2}
	fmt.Println(a)
	fmt.Println(len(a))
	fmt.Println(cap(a))
	fmt.Println()

	// unlike arrays, you can reassign slice of different size
	a = []int{0, 1, 2, 3}
	fmt.Println(a)
	fmt.Println(cap(a))
	a = append(a, 4)
	fmt.Println(a)
	fmt.Println(cap(a))
	a = append(a, 5, 6)
	fmt.Println(a)
	fmt.Println(cap(a))
	fmt.Println()
	// 0, 1, 2, 4, 8, 16...

	// declare slices of a certain size
	b := make([]int, 5)
	fmt.Println(b)
	fmt.Println(cap(b))
	fmt.Println()
	// maps and channel also use make() as new() in other languages
	// sub

	// a slice of a slice
	fmt.Println(a)
	a = a[0:7]
	fmt.Println(a)
	a = a[0:8]
	fmt.Println(a)
	// a = a[-1:8] // error
	a = a[1:]
	fmt.Println(a)
	a = a[0:]
	fmt.Println(a)
	a = a[:2]
	fmt.Println(a)
	a = a[:3]
	fmt.Println(a)
	a = a[1:3]
	fmt.Println(a)
	// can use slice as a stack or queue with these operations

	// fmt.Println(a == a), create our own equals function
	// we will talk about functions in another video

	// you can compare with nil, as reference type zero value
	// is nil, as we talked about in variable declaration vid
	if a == nil {}
	var c []int
	c = nil
	c = []int(nil)
	fmt.Println(c == nil)


}
