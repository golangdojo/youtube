package main

import (
	"fmt"
	"html/template"
	"os"
)

type page struct {
	// capitalized to be used, for more, check out my scope video
	Header   string
	Paragraph string
}

func main() {
	p := page{"Wallace", "I am a ninja"}
	templatePath := "C:/Users/Wallace/workspace/src/golangdojo.com/golangdojo/youtube/basics/ch4htmltemplates/prep/template.html"
	t, err := template.New("template.html").ParseFiles(templatePath)
	if err != nil {
		fmt.Println(err)
	}
	err = t.Execute(os.Stdout, p)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println()
	fmt.Println()

	//p = page{"Wallace", "<b>I'm a ninja"}
	//err = t.Execute(os.Stdout, p)
	//if err != nil {
	//	fmt.Println(err)
	//}
}
