package main

import (
	"fmt"
	"html/template"
	"os"
)

type page struct {
	Header string
	Paragraph string
}

func main() {
	p := page{"Wallace", "<b>I am a Ninja"}
	fmt.Println(p)
	templatePath := "C:/Users/Wallace/workspace/src/golangdojo.com/golangdojo/youtube/basics/ch4htmltemplates/demo/template.html"
	t, error := template.New("template.html").ParseFiles(templatePath)
	if error != nil {
		fmt.Println(error)
	}
	error = t.Execute(os.Stdout, p)
	if error != nil {
		fmt.Println(error)
	}
	fmt.Println()
	fmt.Println("Hello World\\n")


}
