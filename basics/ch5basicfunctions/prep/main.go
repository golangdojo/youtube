package main

import (
	"errors"
	"fmt"
)

// main function
func main() {
	fmt.Println("Hello 世界")
}

// no overloading
func overloading() {
	fmt.Println("Overloading")
}

// func overloading(s string) {}

// variadic function, kind of overloading
func variadic(integers ...int) {
	for integer := range integers {
		fmt.Println(integer)
	}
}

// return statement
func sum(a int, b int) int {
	return a + b
}

// multiple return type, with error and others
// not sure how prevalent this would be in production
// from a java boi point of view
// may be seen as bad coding style if you have too many return types
func nameLen(name string) (string, int) {
	return name, len(name)
}

func greeting(name string) (string, error) {
	if len(name) == 0 {
		return "", errors.New("empty name")
	}
	return "Hello, " + name, nil
}

// with ok if don't care about the error
func greetingOk(name string) (string, bool) {
	if len(name) == 0 {
		return "", false
	}
	return "Hello, " + name, true
}

// recursion
func fibonacci(i int) int {
	if i == 0 {
		return 0
	} else if i == 1 {
		return 1
	} else {
		return fibonacci(i-1) + fibonacci(i-2)
	}
}

// passed by value if without pointers
func withoutPointer(i int) {
	i = i + 1
}
func withPointer(i *int) {
	*i = *i + 1
}




