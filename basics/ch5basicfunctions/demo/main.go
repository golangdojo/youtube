package main

import (
	"errors"
	"fmt"
)

func main() {
	variadic(1, 2, 3, 4, 5)
	fmt.Println(sum(1, 2))
	fmt.Println(nameLength("Wallace"))

	result, error := greeting("")
	if error != nil {
		fmt.Println(error)
	} else {
		fmt.Println(result)
	}

	resultOk, ok := greetingOk("")
	if ok {
		fmt.Println(resultOk)
	}

	fmt.Println(fibonacci(10))

	i := 1
	fmt.Println(i)
	withoutPointer(i)
	fmt.Println(i)
	withPointer(&i)
	fmt.Println(i)
}

func function() {
	fmt.Println("Hello 世界")
}
//
//func function(integer int) {
//	fmt.Println("Hello 世界", integer)
//}

func variadic(integers ...int) {
	for _, integer := range integers {
		fmt.Println(integer)
	}
}

func sum(a, b int) int {
	return a + b
}

func nameLength(name string) (string, int) {
	return name, len(name)
}

func greeting(name string) (string, error) {
	if len(name) == 0 {
		return "", errors.New("Empty name! This is not good!")
	} else {
		return "Hello " + name, nil
	}
}

func greetingOk(name string) (string, bool) {
	if len(name) == 0 {
		return "", false
	} else {
		return "Hello " + name, true
	}
}

func fibonacci(i int) int {
	if i == 0 {
		return 0
	} else if i == 1 {
		return 1
	} else {
		return fibonacci(i - 1) + fibonacci(i - 2)
	}
}

func withoutPointer(i int) {
	i = i + 1
}

func withPointer(i *int) {
	*i = *i + 1
}
